# Interactive Map

![Image de présentation](img/image_presentation.png)

## Presentation
This is a web application coded in vanilla javascript, easy to start and manage.
At first you will need data such as your map image in SVG extension and your JSON
datas about the differents office of your "company".
I will show you how to organize your datas.

## Organization
### JSON
#### conf.txt
- To start you have the choice for the catch method of the JSON file, you can choose
to upload datas on URL and write it in the conf.txt, everything will be explained.
Or you can already specifie in the conf.txt, a path to your local datas.

#### json format
- The JSON should have a specifie form like that :

``` json
[
  {
      "libelle": "B101",
      "nombre_place": 2,
      "etage": 1,
      "personnels": [
        {
          "nom": "Dung",
          "prenom": "Axel",
          "equipe": "SISR-LORIA",
          "employeur": "UL",
          "telephone": null,
          "date_debut": "16-04-2024",
          "date_fin": "16-05-2024"
        },
        {
          "nom": "Pierrot",
          "prenom": "Nathan",
          "equipe": "SYNALP",
          "employeur": "UL",
          "telephone": null,
          "date_debut": "17-04-2024",
          "date_fin": "17-05-2024"
        }
      ],
      "commentaire": null,
      "date_commentaire": null
  },
  {
      "libelle": "B102",
      "nombre_place": 3,
      "etage": 1,
      "personnels": [
        {
          "nom": "John",
          "prenom": "Doe",
          "equipe": "Team 1",
          "employeur": "X",
          "telephone": null,
          "date_debut": "16-04-2024",
          "date_fin": "16-05-2024"
        }
      ],
      "commentaire": null,
      "date_commentaire": null
  }
]
```

- If any information is missing, it's not a problem, the application will not crash,
but it is recommended that all the keys are here, you can put it empty if you want.

### SVG
#### conf.txt
- Now in the conf.txt, you have to write the differents name of your buildings and
how many floors they have, like the others configurations, everything will be explained.

#### svg format
- If you doesn't have the map in a svg extension, good luck to build it, a lot of web site
allow you to do this.

- When you have it, you will affect an attribute to every Nodes wich are an office.
The attribute name is 'data-office' and the value is the 'libelle' of the office in
your JSON, without this 2 informations, it doesn't work, it's like the ID of the Node.

- I leave you an example just below :

``` html
<svg width="100%" height="100%" viewBox="0 50 600 300">
    <rect data-office="B101" x="199.30001" y="161.49166" width="73.91667" height="62.33333"/>
    <text  font-size="18" y="200.25" x="215.2">B101</text>
</svg>
```

### ADMIN
- I think that will interest you to have an administration and guest version of the web application.
To choice the version, open the conf.txt file and put the admin variable to true or false.
Don't forget to specifie the URL of the JSON datas because the format is not the same :

``` json
[
  {
      "libelle": "B101",
      "nombre_place": 2,
      "etage": 1,
      "personnels": [
        {
          "nom": "Dung",
          "prenom": "Axel",
          "equipe": "SISR-LORIA",
          "statut": "non-permanent"
        },
        {
          "nom": "Pierrot",
          "prenom": "Nathan",
          "equipe": "SYNALP",
          "statut": "non-permanent"
        }]
  },
  {
      "libelle": "B102",
      "nombre_place": 3,
      "etage": 1,
      "personnels": [
        {
          "nom": "John",
          "prenom": "Doe",
          "equipe": "Team 1",
          "statut": "permanent"
        }]
]
```

- You can use admin data in guest mode but it will not be secure because the
the person can access the data in their cookie.

## What to do, if ...
### What to do, if a team is new

A team will coming soon, and you don't know how to integrate it in your project.
Open the "equipes.txt" file and add a line like this :

``` txt
<Name of the team>=<Color for the team mode>=<Departement of the team>
Example : VERIDIS=paleturquoise=D2
```

Color must be understood by CSS.
All this information is not necessary for the proper functioning of the application

### What to do, if I want to change the area of an office

You'r not agree with the area of an office.
No problem, open the "superficie.txt" file, press CTRL-F and search the office
in question and change the line like that :

``` txt
<Name of the office>=<Area of the office>
Example : A107=20.54
```

### What to do, if an new building is comming

Open the "conf.txt" file, go at the bottom of the file and add a line like this :

``` txt
batiment=<Name of bulding>=<Floors number>
batiment=d=7
```






