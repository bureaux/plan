"use strict";
import * as jq from "../jq.js";

import * as viewSvg from "../views/viewSvg.js";
import * as viewInfos from "../views/viewInfos.js";
import * as viewCommentaire from "../views/viewCommentaire.js";
import * as viewTampon from "../views/viewTampon.js";
import * as viewMenu from "../views/viewMenu.js";
import * as viewMap from "../views/viewMap.js";

import * as controlBuilding from "../controllers/controlBuilding.js";
import * as controlFloor from "../controllers/controlFloor.js";
import * as controlMode from "../controllers/controlMode.js";
import * as controlYear from "../controllers/controlDate.js";
import * as controlSearch from "../controllers/controlSearch.js";
import * as controlOption from "../controllers/controlMenu.js";
import * as controlDragAndDrop from "../controllers/controlDragAndDrop.js";
import * as controlLegend from "../controllers/controlLegend.js";
import * as controlExportSvg from "../controllers/controlExportSvg.js";
import * as controlHide from "../controllers/controlHide.js";
import * as controlFastSave from "../controllers/controlFastSave.js";
import * as controlComment from "../controllers/controlComment.js";

import * as json from "../json/json.js";

import * as displayColor from "../displays/displayColor.js";
import * as displayTeam from "../displays/displayTeam.js";
import * as displayNotification from "../displays/displayNotification.js";

// Variable admin
export let admin;

// Variables courantes
export let building, floor, mode, year, month, currentOffice, searchIterator, equipes, jsonDatas, tauxPerm, tauxNonPerm,
    nameSimu, logs, currentMap, correctifs;


/**
 * Initialisation de l'application
 * initAdmin() : on récupère la variable admin (true = mode simulation, false = mode visualisation)
 * json.init() : on initialise les données json
 * load() : on charge les données depuis les cookies (Si ils existent sinon on prend des valeurs par défaut)
 * initTeam() : on associe une couleur à chaque équipe
 * view & control : on initialise les vues et les contrôleurs
 */
export async function init() {
    reset();
    
    await initAdmin();
    await load();

    initTeam();

    await viewCommentaire.init();
    await viewMenu.init();
    await viewTampon.init();
    await viewSvg.init();

    await viewCommentaire.update();
    await viewTampon.update();
    await viewInfos.update();
    await viewMap.update();

    await controlBuilding.init();
    await controlFloor.init();
    controlMode.init();
    controlYear.init();
    controlSearch.init();
    controlOption.init();
    controlDragAndDrop.initBureau();
    controlLegend.init();
    controlExportSvg.init();
    controlHide.init();
    controlFastSave.init();
    controlComment.init();

    await checkBuilding();
}

/**
 * Initialise la variable admin
 * On récupère la variable admin depuis le fichier admin.txt
 * On la garde dans la variable admin
 * true = mode simulation
 * false = mode visualisation
 * @return {Promise<unknown>}
 */
async function initAdmin() {
    return new Promise((resolve) => {
        fetch("../../conf.txt")
            .then(response => {
                return response.text();
            })
            .then(data => {
                let tab = data.split("\n");
                tab.forEach(line => {
                    if (line.includes("=") && !line.includes("#")) {
                        let key = line.split("=")[0];
                        let value = line.split("=")[1];
                        if (key === "admin") {
                            value = value.replace(/\s/g, '');
                            admin = value === "true";
                        }
                    }
                })
            })
            .then(() => {
                resolve();
            });
    });
}

/**
 * Initialise les équipes avec des couleurs
 * jq : '.[] | .personnels | map(.) [] | .equipe'
 * Je remplie un dictionnaire avec une équipe comme clé et une couleur comme valeur
 * (Attention, il faut vérifier que les équipes sont uniques)
 */
function initTeam() {
    equipes = new Map();
    fetch("../datas/equipes.txt")
        .then(response => {
            return response.text();
        })
        .then(data => {
            let tab = data.split("\n");
            tab.forEach(line => {
                if (line.includes("=") && !line.includes("#")) {
                    let tabEquipe = line.split("=");
                    equipes.set(tabEquipe[0], tabEquipe[1]);
                }
            })
        });
}

/**
 * Extrait le numéro de bureau d'un noeud path ou rect
 * @param node
 * @returns {string}
 */
export function extractNumBureau(node) {
    if (node.getAttribute("data-office") != null) {
        return node.getAttribute("data-office");
    }
}

/**
 * Retourne le bureau en json correspondant au numéro de bureau sur le DOM
 * jq : '.[] | .[] | select(.libelle == "Bureau")'
 * @param numBureau
 * @returns {*}
 */
export function findOfficeJson(numBureau) {
    const filter = jq.compile('.[] | .[] | select(.libelle == "' + numBureau + '")');
    const filters = filter(jsonDatas);
    for (let v of filters) {
        return v;
    }
}

/**
 * Retourne le Node path ou rect correspondant au numéro de bureau
 * @param numBureau
 * @returns {*}
 */
export function findOfficeNode(numBureau) {
    if (numBureau !== undefined) {
        return document.getElementById("svg__" + currentMap).querySelector('[data-office="' + numBureau + '"]');
    }
}


/**
 * Retourne le Node texte correspondant au numéro de bureau
 * @param numBureau
 * @param parent
 * @returns {*}
 */
export function findTextNode(numBureau, parent) {
    let res;
    let nodes = Array.from(parent.getElementsByTagName("text"));
    nodes.forEach(node => {
        if (node.textContent === numBureau) {
            res = node;
            return res;
        }
    });
    return res;
}

/**
 * Retourne l'équipe d'un bureau en json
 * Si le bureau contient plusieurs équipes, on renvoie "Equipe Mixte"
 * A savoir que si nous sommes en mode simulation, on ne possède pas
 * les dates de fin de contrat, on les affiche donc
 * @param bureauJson
 * @return {*|string|null}
 */
export function getEquipe(bureauJson) {
    let tabEquipe = [];
    if (bureauJson && bureauJson.personnels.length !== 0) {
        bureauJson.personnels.forEach(person => {
            if (checkDate(person) || !admin) {
                tabEquipe.push(person.equipe);
            }
        });
    } else {
        return null;
    }
    for (let i = 0; i < tabEquipe.length; i++) {
        for (let j = i + 1; j < tabEquipe.length; j++) {
            if (tabEquipe[i] !== tabEquipe[j]) {
                return tabEquipe.filter((item, index) => tabEquipe.indexOf(item) === index);
            }
        }
    }
    return tabEquipe[0];
}

/**
 * Retourne la couleur d'une équipe
 * @param equipe
 * @return {*}
 */
export function getColor(equipe) {
    return equipes.get(equipe);
}

/**
 * Retourne le departement de l'équipe d'une personne
 * @param person
 * @return {Promise<unknown>}
 */
export async function getDepartement(person) {
    return new Promise((resolve) => {
        let departement;
        fetch("../datas/equipes.txt")
            .then(response => {
                return response.text();
            })
            .then(data => {
                let tab = data.split("\n");
                tab.forEach(line => {
                    if (line.includes("=") && !line.includes("#")) {
                        let tabEquipe = line.split("=");
                        if (person.equipe === tabEquipe[0]) {
                            departement = tabEquipe[2];
                        }
                    }
                })
            })
            .then(() => {
                if (departement === undefined) {
                    departement = "inconnu";
                }
                resolve(departement);
            });
    });
}

/**
 * Retourne le taux d'occupation d'un bureau
 * A savoir :
 * - Un permanent a un taux d'occupation de 1
 * - Un non permanent a un taux d'occupation de 0.666
 * - On reconnait un permanent par la date de fin de contrat (2099)
 * @param bureauJson
 * @return {number}
 */
export function getTauxBureau(bureauJson) {
    let res = 0;
    if (bureauJson && bureauJson.personnels.length !== 0) {
        bureauJson.personnels.forEach(person => {
            let anneeFin;
            if (person.date_fin) {
                anneeFin = person.date_fin.split("-")[2];
            }
            if (checkDate(person) || !admin) {
                if (anneeFin === "2099" || person.statut === "permanent") {
                    res += parseFloat(person.taux_occupation || bureauJson.taux_perm || tauxPerm);
                } else if (anneeFin !== "2099" || person.statut === "non-permanent") {
                    res += parseFloat(person.taux_occupation || bureauJson.taux_non_perm || tauxNonPerm);
                }
            }
        });
    }
    return res
}

/**
 * Recherche le bureau d'une personne à partir d'un champ de recherche
 * On split le texte en deux parties pour essayer de trouver le nom et le prénom
 * Sinon on recherche sur le nom puis le prénom
 * jq : '.[] | .[] | { libelle: .libelle, personnels: .personnels}'
 * @param text
 * @return {*[]}
 */
export function searchPerson(text) {
    text = text.toLowerCase();
    let tabNom = text.split(" ");
    let tabResult = [];

    const filter = jq.compile('.[] | .[] | { libelle: .libelle, personnels: .personnels}');
    const filters = filter(jsonDatas);
    for (let v of filters) {
        let bureau = v.libelle;
        let personnels = v.personnels;
        for (let person of personnels) {
            if (checkDate(person) || !admin) {
                if (tabNom.length === 2) {
                    if (tabNom[0].includes(person.nom.toLowerCase()) && tabNom[1].includes(person.prenom.toLowerCase()) || tabNom[0].includes(person.prenom.toLowerCase()) && tabNom[1].includes(person.nom.toLowerCase())) {
                        tabResult.push([bureau, person.nom + " " + person.prenom]);
                    }
                } else if (tabNom.includes(person.nom.toLowerCase()) || tabNom.includes(person.prenom.toLowerCase())) {
                    tabResult.push([bureau, person.nom + " " + person.prenom]);
                }
            }
        }
    }
    return tabResult;
}

/**
 * Recherche les bureaux d'une équipe à partir d'un champ de recherche
 * A savoir :
 * - Les noms d'équipes sont en majuscule dans le json (d'où le toUpperCase)
 * - Je partionne les bureaux par batiment et étage car on ne peut pas afficher tous les étages/batiment en même temps
 * jq : '.[] | .[] | select(.personnels[] | .equipe == "Equipe") | .libelle'
 * @param text
 * @return {*[]}
 */
export function searchTeam(text) {
    text = text.toUpperCase();
    let tabResult = [];

    const filter = jq.compile('.[] | .[] | select(.personnels[] | .equipe == "' + text + '") | .libelle');
    const filters = filter(jsonDatas);
    for (let v of filters) {
        if (!tabResult.includes(v)) {
            tabResult.push(v);
        }
    }

    // Partitionnement des bureaux par batiment ET étage
    tabResult.sort();
    let sousListes = [];
    for (let element of tabResult) {
        if (sousListes.length === 0 || !sousListes[sousListes.length - 1][0].startsWith(element.substring(0, 2))) {
            sousListes.push([element]);
        } else {
            sousListes[sousListes.length - 1].push(element);
        }
    }
    tabResult = sousListes;
    return tabResult;
}

/**
 * Auto-complétion pour les personnes
 * jq : '.[] | .[] | .personnels | map(.) | .[]'
 * @param text
 * @return {*[]}
 */
export function autoCompletionPerson(text) {
    if (text.length === 0) return [];
    text = text.toLowerCase();
    let tabNom = text.split(" ");
    let tabResult = [];

    const filter = jq.compile('.[] | .[] | .personnels | map(.) | .[]');
    const filters = filter(jsonDatas);
    for (let p of filters) {
        if (checkDate(p) || !admin) {
            if (tabNom.length === 2) {
                if (tabNom[0].includes(p.nom.toLowerCase()) && tabNom[1].includes(p.prenom.toLowerCase()) || tabNom[0].includes(p.prenom.toLowerCase()) && tabNom[1].includes(p.nom.toLowerCase())) {
                    tabResult.push(p.nom + " " + p.prenom);
                }
            } else if (p.nom.toLowerCase().includes(text) || p.prenom.toLowerCase().includes(text)) {
                tabResult.push(p.nom + " " + p.prenom);
            }
        }
    }

    return tabResult;
}

/**
 * Auto-complétion pour les équipes
 * jq : '.[] | .[] | .personnels | map(.) | .[]'
 * @param text
 * @return {*[]}
 */
export function autoCompletionTeam(text) {
    if (text.length === 0) return [];
    text = text.toUpperCase();
    let tabResult = [];

    const filter = jq.compile('.[] | .[] | .personnels | map(.) | .[]');
    const filters = filter(jsonDatas);
    for (let p of filters) {
        if (checkDate(p) || !admin) {
            if (p.equipe.includes(text)) {
                if (!tabResult.includes(p.equipe)) tabResult.push(p.equipe);
            }
        }
    }
    return tabResult;
}

/**
 * Vérifie si la personne est encore dans le bureau
 * @param personJson
 * @return {boolean}
 */
export function checkDate(personJson) {
    if (personJson === undefined || personJson.length === 0 || !personJson.date_debut || !personJson.date_fin) return false;
    let anneeDebut = personJson.date_debut.split("-")[2];
    let anneeFin = personJson.date_fin.split("-")[2];
    let moisDebut = personJson.date_debut.split("-")[1];
    let moisFin = personJson.date_fin.split("-")[1];

    if (anneeDebut < year && anneeFin > year) {
        return true;
    } else if (anneeDebut == year && anneeFin == year) {
        if (moisDebut <= month && moisFin >= month) {
            return true;
        }
    } else if (anneeFin == year) {
        if (moisFin >= month) {
            return true;
        }
    } else if (anneeDebut == year) {
        if (moisDebut <= month) {
            return true;
        }
    }
    return false;
}

/**
 * Coupe la personne du bureau pour la mettre en temporaire
 * @param person
 * @param numBureau
 */
export async function cut(person, numBureau) {
    jsonDatas.forEach(batiment => {
        batiment.forEach(bureau => {
            if (bureau.libelle === numBureau) {
                bureau.personnels.forEach(p => {
                    if (p.nom === person.nom && p.prenom === person.prenom) {
                        bureau.personnels.splice(bureau.personnels.indexOf(p), 1);
                        pushTampon(person);
                    }
                })
            }
        })
    })
    await updateViews();
}

/**
 * Met la person en paramètre dans la zone tampon (pratique pour la zone non-affecté)
 * @param person
 */
export function pushTampon(person) {
    jsonDatas[jsonDatas.length - 1][0].personnels.push(person);
    displayNotification.displaySuccess(person.nom + " " + person.prenom + " a été placé dans la zone tampon");
    addLogs(person.nom + " " + person.prenom + " a été placé dans la zone tampon");
}

/**
 * Supprime la personne pour la mettre dans la zone non affectée
 * @param person
 * @param numBureau
 */
export async function deletePerson(person, numBureau) {
    jsonDatas.forEach(batiment => {
        batiment.forEach(bureau => {
            if (bureau.libelle === numBureau) {
                bureau.personnels.forEach(p => {
                    if (p.nom === person.nom && p.prenom === person.prenom) {
                        bureau.personnels.splice(bureau.personnels.indexOf(p), 1);
                        pushZoneNA(person);
                    }
                })
            }
        })
    })
    addLogs(person.nom + " " + person.prenom + " a été supprimé");
    await updateViews();
}

/**
 * Met la person en paramètre dans la zonena
 * @param person
 */
function pushZoneNA(person) {
    jsonDatas[jsonDatas.length - 2][0].personnels.push(person);
    displayNotification.displayWarning(person.nom + " " + person.prenom + " a été supprimé");
    addLogs(person.nom + " " + person.prenom + " a été supprimé");
}

/**
 * Affiche le map en couleur en fonction du mode choisi
 * @param numBureau
 */
export function color(numBureau) {
    let bureauJson = findOfficeJson(numBureau);
    let bureauNode = findOfficeNode(numBureau);
    if (mode === "color") displayColor.display(bureauJson, bureauNode);
    if (mode === "team") displayTeam.display(bureauJson, bureauNode);
}

/**
 * Focus sur un bureau, tous les autres bureaux sont en blanc
 * @param numBureau
 * @param newBureauNode
 * @param bureauNode
 */
export function focusOneOffice(numBureau, newBureauNode, bureauNode) {
    let newTextNode = findTextNode(numBureau, document.getElementById("svg__" + currentMap));
    displayColor.display(findOfficeJson(numBureau), newBureauNode, newTextNode);
    // On remplace le bureau de la personne par le clone dans le dom
    bureauNode.parentNode.replaceChild(newBureauNode, bureauNode);
}

/**
 * Regarde à partir du fichier de configuration combient d'étages sont disponibles pour un batiment
 * Il grise les étages non disponibles et les rends non cliquables
 * @return {Promise<unknown>}
 */
export async function checkBuilding() {
    const reponse = await fetch("../../conf.txt");
    const data = await reponse.text();
    let tab = data.split("\n");
    return new Promise((resolve) => {
        tab.forEach(line => {
            if (line.includes("=") && !line.includes("#")) {
                let word = line.split("=")[0];
                if (word === "batiment") {
                    let key = line.split("=")[1];
                    let value = line.split("=")[2] - 1;
                    if (value < floor && key === building) {
                        document.getElementById(document.getElementById("floor-" + value).htmlFor).checked = true;
                        setFloor(value);
                    }
                    if (key === building) {
                        let floors = document.getElementById("floors");
                        for (let i = 0; i <= floors.getElementsByTagName("input").length - 1; i++) {
                            if (i > value) {
                                document.getElementById("floor-" + i).style.opacity = "0.5";
                                document.getElementById("floor-" + i).style.pointerEvents = "none";
                            } else {
                                document.getElementById("floor-" + i).style.opacity = "1";
                                document.getElementById("floor-" + i).style.pointerEvents = "auto";
                            }
                        }
                    }
                }
            }
        });
        resolve();
    });
}

/**
 * Retourne la superficie d'un bureau
 * @param numBureau
 * @return {Promise<void>}
 */
export async function getSuperficie(numBureau) {
    return new Promise((resolve) => {
        let superficie;
        fetch("../datas/superficies.txt")
            .then(response => {
                return response.text();
            })
            .then(data => {
                let tab = data.split("\n");
                tab.forEach(line => {
                    let tabBureau = line.split("=");
                    if (tabBureau[0] === numBureau) {
                        superficie = tabBureau[1];
                    }
                });
            })
            .then(() => {
                if (superficie === undefined) {
                    superficie = "inconnue";
                } else {
                    superficie += " m²";
                }
            })
            .then(() => {
                resolve(superficie);
            });
    })
}


/**
 * Sauvegarde les données dans le local storage
 */
export function save() {
    if (admin) {
        let data = {
            building: building,
            floor: floor,
            mode: mode,
            year: year,
            month: month,
            currentOffice: currentOffice,
            jsonDatas: jsonDatas,
            tauxPerm: tauxPerm,
            tauxNonPerm: tauxNonPerm,
            nameSimu: nameSimu,
            logs: logs,
            correctifs: correctifs
        };
        localStorage.setItem("data", JSON.stringify(data));
    }
}

/**
 * Charge les données depuis le local storage
 */
export async function load() {
    let data = JSON.parse(localStorage.getItem("data")) || {};
    building = data.building || "b";
    floor = data.floor || "1";
    mode = data.mode || "color";
    year = data.year || new Date().getFullYear();
    month = data.month || new Date().getMonth() + 1;
    currentOffice = data.currentOffice || "info-accueil";
    jsonDatas = data.jsonDatas || await json.resetJson();
    tauxPerm = (data.tauxPerm !== undefined) ? data.tauxPerm : 1.00;
    tauxNonPerm = (data.tauxNonPerm !== undefined) ? data.tauxNonPerm : 0.66;
    nameSimu = data.nameSimu || "nouvelle";
    logs = data.logs || [];
    currentMap = 0;
    correctifs = data.correctifs || [];
}

/**
 * Setters
 */
export async function setCurrentOffice(numBureau) {
    currentOffice = numBureau;
    await viewInfos.update();
    await viewCommentaire.update();
    save();
}

export function setBuilding(b) {
    building = b;
    save();
}

export function setFloor(f) {
    floor = f;
    save();
}

export function setMode(m) {
    mode = m;
    save();
}

export async function setDate(y, m) {
    year = y;
    month = m;
    await updateViews();
    save();
}

export function setSearchIterator(i) {
    searchIterator = i;
}

export async function setJsonDatas(j) {
    jsonDatas = j;
    await updateViews();
    save();
}

export function setListTemp(l) {
    jsonDatas[jsonDatas.length - 1][0].personnels = l;
}

export function setTaux(tP, tNP) {
    tauxPerm = parseFloat(tP);
    tauxNonPerm = parseFloat(tNP);
    save();
}

export async function setNameSimu(n) {
    nameSimu = n;
    save();
    await viewMap.update();
}

export function setCurrentMap(m) {
    currentMap = m;
}

export function addCorrectifs(c) {
    correctifs.push(c);
    save();
}

export function resetCorrectifs() {
    correctifs = [];
    save();
}

export function addLogs(log) {
    logs.push("[" + new Date().toLocaleString() + "] " + log);
}

export async function setAll(b, f, m, y, c) {
    return new Promise((resolve) => {
        building = b;
        floor = f;
        mode = m;
        year = y;
        currentOffice = c;

        document.getElementById(document.getElementById("building-" + building).htmlFor).checked = true;
        document.getElementById(document.getElementById("floor-" + floor).htmlFor).checked = true;
        document.getElementById(document.getElementById("mode-" + mode).htmlFor).checked = true;

        checkBuilding();

        updateViews().then(() => {
            resolve();
        });
        save();
    });
}

export async function reset() {
    jsonDatas = [];
    jsonDatas = await json.resetJson();
    localStorage.clear();
    correctifs = [];
    logs = [];
    tauxPerm = 1.00;
    tauxNonPerm = 0.66;
    setCurrentMap(0);
    Array.from(document.getElementsByTagName("svg")).forEach(svg => {
        if (svg.id !== "svg__0") svg.remove();
    });
    await updateViews();
    displayNotification.displayWarning("Réinitialisation des données");
    save();
}

export async function updateViews() {
    await viewInfos.update();
    await viewSvg.update();
    await viewTampon.update();
    await viewCommentaire.update();
    save();
}
