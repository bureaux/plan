"use strict";
import * as model from "../modules/model/model.js";

// Initialise l'application au chargement de la page
window.addEventListener("load", async function () {
    await model.init();
});
