"use strict";
import * as model from "../model/model.js";
import * as displayOnePerson from "../displays/displayOnePerson.js";
import * as displayOneTeam from "../displays/displayOneTeam.js";
import * as displaySuggestion from "../displays/displaySuggestion.js";
import * as displayNotification from "../displays/displayNotification.js";

/**
 * Initialise le mode de recherche
 */
export function init() {
    let searchNode = document.getElementById("search");
    let searchDiv = document.getElementById("search-bottom");
    searchDiv.style.pointerEvents = "none";
    let autoComplete = document.getElementById("autocomplete");

    searchNode.addEventListener('keyup', function (event) {
        const key = event.key;
        if (key !== "Enter") {
            if (searchNode.value.length === 0) {
                searchDiv.style.opacity = "0";
                searchDiv.style.pointerEvents = "none";
                autoComplete.style.display = "none";
            } else {
                autoComplete.style.display = "block";
                displaySuggestion.display(searchNode.value);
            }
        }
    });

    searchNode.addEventListener("keypress", function (event) {
        if (event.key === "Enter") {
            autoComplete.style.display = "none";
            searchDiv.style.opacity = "1";
            searchDiv.style.pointerEvents = "auto";
            searchForward(searchNode.value);
        }
    });

    document.getElementById("search-button").addEventListener("click", function () {
        autoComplete.style.display = "none";
        searchDiv.style.opacity = "1";
        searchDiv.style.pointerEvents = "auto";
        searchForward(searchNode.value);
    });

    let searchPrev = document.getElementById("search-button-prev");
    let searchNext = document.getElementById("search-button-next");
    searchPrev.addEventListener("click", function () {
        searchBackward(searchNode.value);
    });

    searchNext.addEventListener("click", function () {
        searchForward(searchNode.value);
    });

    document.addEventListener("click", function (event) {
        if (event.target.id !== "autocomplete" && event.target.id !== "search") {
            autoComplete.style.display = "none";
        }
    });
}

function searchForward(text) {
    model.setSearchIterator(model.searchIterator + 1);
    search(text);
}

function searchBackward(text) {
    model.setSearchIterator(model.searchIterator - 1);
    search(text);
}

function search(text) {
    if (model.searchPerson(text).length > 0) {
        displayOnePerson.display(model.searchPerson(text));
    } else if (model.searchTeam(text).length > 0) {
        displayOneTeam.display(model.searchTeam(text));
    } else {
        document.getElementById("search-nb").innerHTML = "0 / 0";
        displayNotification.displayError("Aucun résultat trouvé");
    }
}