"use strict";
import * as displayFormulaire from "../displays/displayFormulaire.js";
import * as model from "../model/model.js";

/**
 * Initialise le bouton d'export svg
 */
export function init() {
    let button = document.getElementById("button__exporter");
    if (model.admin) {
        button.addEventListener("click", function () {
            displayFormulaire.displayExportSvg();
        });
    } else {
        button.remove();
    }
}