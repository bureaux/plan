"use strict";

import * as displayFormulaire from "../displays/displayFormulaire.js";
import * as model from "../model/model.js";
import {displayError, displaySuccess} from "../displays/displayNotification.js";
import * as viewZoneNA from "../views/viewZoneNA.js";
import * as viewLogs from "../views/viewLogs.js";
import * as json from "../json/json.js";
import * as jsondiffpatch from 'https://esm.sh/jsondiffpatch@0.6.0';


/*
 * Initialise les boutons du menu
 */
export function init() {
    if (model.admin) {
        // Gestion de l'ombre et de l'affichage des options
        let option = document.getElementById("option");
        let optionWrapper = document.getElementById("option__wrapper");
        let ombre = document.getElementById("ombre");

        // Action sur le bouton, affiche les options
        option.addEventListener("click", function () {
            optionWrapper.style.right = "0";
            ombre.style.display = "block";
        });


        // Action sur l'ombre, ferme les options
        ombre.addEventListener("click", function () {
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
        });

        // Initialisation des boutons de gestion de données
        let save = document.getElementById("json__save");
        let load = document.getElementById("json__load");
        let reset = document.getElementById("json__reset");

        // Action sur le bouton de sauvegarde, sauvegarde les données
        save.addEventListener("click", function () {
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
            displayFormulaire.displayExportData();
        });

        // Action sur le bouton d'import, clique sur le bouton de chargement
        document.getElementById("json__import").addEventListener("click", function () {
            load.click();
        });

        // Action sur le bouton d'import de fichier, importe les données
        load.addEventListener("change", function () {
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
            if (load.files.length === 1) {
                let reader = new FileReader();
                reader.readAsText(load.files[0]);
                reader.onload = async function (event) {
                    if (load.files[0].name.split(".")[1] !== "json") {
                        displayError("Le fichier " + load.files[0].name + " n'est pas un fichier json");
                    } else {
                        let json = JSON.parse(event.target.result);
                        await model.setJsonDatas(json);
                        await model.setNameSimu(load.files[0].name.split(".")[0]);
                        model.resetCorrectifs();
                        model.addCorrectifs(load.files[0].name);
                        displaySuccess("Les données du fichier " + load.files[0].name + " ont été importées avec succès");
                    }
                };
            }
        });

        // Action sur le bouton de reset, reset les données
        reset.addEventListener("click", async function () {
            load.value = "";
            await model.reset();
            await model.setNameSimu("nouvelle")
        });

        let tauxGlobal = document.getElementById("taux__global");
        // Action sur le bouton de taux globaux, affiche les taux globaux
        tauxGlobal.addEventListener("click", function () {
            displayFormulaire.displayTauxGlobaux();
        });

        // Action sur la zone de non affectation, affiche la zone de non affectation
        document.getElementById("zone__nn").addEventListener("click", async function () {
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
            await viewZoneNA.update();
        });

        // Action sur le bouton de logs, affiche les logs
        document.getElementById("logs").addEventListener("click", async function () {
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
            await viewLogs.update();
        });

        let importCorrectif = document.getElementById("correctif__import");
        // Action sur le bouton d'import de correctif
        importCorrectif.addEventListener("click", function () {
            let input = document.createElement("input");
            input.type = "file";
            input.accept = ".json";
            input.click();
            input.addEventListener("change", async function () {
                let reader = new FileReader();
                reader.readAsText(input.files[0]);
                reader.onload = async function (event) {
                    // Récupère le json actuel, le reset et le json à appliquer
                    let jsonUpload = JSON.parse(event.target.result);
                    let jsonReset = await json.resetJson();
                    let jsonActu = model.jsonDatas;

                    // Faits les différences et applique le correctif avec la librairie jsondiffpatch
                    let diff = jsondiffpatch.diff(jsonReset, jsonUpload);
                    console.log(diff);
                    console.log(jsonReset);
                    console.log(jsonUpload);
                    // if (model.correctifs.includes(input.files[0].name)) {
                    //     displayError("Le correctif " + input.files[0].name + " a déjà été appliqué");
                    // } else {
                    model.addCorrectifs(input.files[0].name);
                    jsondiffpatch.patch(jsonActu, diff);

                    // Met à jour la vue et affiche un message de succès
                    await model.updateViews();
                    displaySuccess("Le correctif " + input.files[0].name + " a été appliqué avec succès");
                    // }
                };
            });
        });

        document.getElementById("a-propos").addEventListener("click", function () {
            displayFormulaire.displayAPropos();
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
        });

        document.getElementById("json2csv").addEventListener("click", function () {
            displayFormulaire.displayJson2Csv();
            optionWrapper.style.right = "-50%";
            ombre.style.display = "none";
        });
    }
}