"use strict";
import * as model from "../model/model.js";
import * as controlLegend from "./controlLegend.js";
import * as displayLegend from "../displays/displayLegend.js";
import * as contolSvg from "./controlSvg.js";
import * as viewSvg from "../views/viewSvg.js";

export function init(parent) {
    if (model.admin) {
        parent.addEventListener('click', async function (event) {
            if (event.target.className === "supprimer__map" || event.target.className === "supprimer__map__img") {
                parent.remove();
                model.setCurrentMap(0);
                parent = document.getElementById("svg__0");
            }
            clear();

            parent.style.opacity = "1";
            parent.style.border = "2px dashed black";
            model.setCurrentMap(parent.id.split("__")[1]);

            if (controlLegend.legendShow) {
                displayLegend.display();
            }

            // Mise à jour des radio boutons
            document.getElementById(document.getElementById("building-" + parent.getAttribute("building")).htmlFor).checked = true;
            document.getElementById(document.getElementById("floor-" + parent.getAttribute("floor")).htmlFor).checked = true;
            document.getElementById(document.getElementById("mode-" + parent.getAttribute("mode")).htmlFor).checked = true;

            model.setBuilding(parent.getAttribute("building"));
            model.setFloor(parent.getAttribute("floor"));
            model.setMode(parent.getAttribute("mode"));

            await viewSvg.update();
        });
    }

    Array.from(parent.getElementsByTagName("path")).forEach(node => {
        contolSvg.initPathRect(node, parent);
    });

    Array.from(parent.getElementsByTagName("rect")).forEach(node => {
        contolSvg.initPathRect(node, parent);
    });

    Array.from(parent.getElementsByTagName("text")).forEach(node => {
        contolSvg.initText(node, parent);
    });

}

function clear() {
    let maps = document.getElementById("map__loria");
    let svg = maps.getElementsByClassName("div__map");
    for (let i = 0; i < svg.length; i++) {
        svg[i].style.opacity = "0.5";
        svg[i].style.border = "2px dashed transparent";
    }
}