"use strict";
import * as model from "../model/model.js";


/**
 * Initialise le bouton de sauvagarde rapide
 */
export function init() {
    let button = document.getElementById("button__fast__save");
    if (model.admin) {
        button.addEventListener("click", function () {
            let jsonString = JSON.stringify(model.jsonDatas);
            let blob = new Blob([jsonString], {type: "application/json"});
            let url = URL.createObjectURL(blob);

            let a = document.createElement("a");
            a.href = url;
            a.download = `save.json`;
            a.title = "save";

            document.body.appendChild(a);
            a.click();

            URL.revokeObjectURL(url);
            document.body.removeChild(a);
        });
    } else {
        button.remove();
    }
}