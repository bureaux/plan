"use strict";
import * as model from "../model/model.js";
import * as viewSvg from "../views/viewSvg.js";

/**
 * Initialise le mode de visualisation
 */
export function init() {
    let modeDefault = document.getElementById("mode-default");
    let modeCouleur = document.getElementById("mode-color");
    let modeEquipe = document.getElementById("mode-team");

    document.getElementById(document.getElementById("mode-" + model.mode).htmlFor).checked = true;

    modeDefault.addEventListener("click", function () {
        model.setMode("default");
        viewSvg.update();
    });

    modeCouleur.addEventListener("click", function () {
        model.setMode("color");
        viewSvg.update();
    });

    modeEquipe.addEventListener("click", function () {
        model.setMode("team");
        viewSvg.update();
    });
}