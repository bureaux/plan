"use strict";
import * as model from "../model/model.js";

/**
 * Fonction principale d'initialisation des différents éléments du svg
 * Rectangles, paths et textes
 * @param node
 * @param parent
 * @param numBureau
 */
function init(node, parent, numBureau) {
    let bureauJson = model.findOfficeJson(numBureau);

    node.addEventListener("click", function () {
        model.setCurrentOffice(numBureau);
    });

    if (bureauJson) {
        let equipe = model.getEquipe(bureauJson);
        let nbPersonnel = model.getTauxBureau(bureauJson);
        let place = bureauJson.nombre_place;
        model.color(numBureau);

        if (model.admin) {
            node.addEventListener("mouseover", function (event) {
                if (document.getElementsByClassName("hover").length === 0) {
                    hoverAction(event, equipe, `${nbPersonnel.toFixed(2)} / ${place}`);
                }
            });
        }

        node.addEventListener("mouseout", function () {
            outAction();
        });

        parent.addEventListener("mouseout", function () {
            outAction();
        });
    }
}

/**
 * Initialise les rectangles et les paths
 * Ce qui change par rapport à initText c'est l'extraction du numéro de bureau
 * @param node
 * @param parent
 */
export function initPathRect(node, parent) {
    let numBureau = model.extractNumBureau(node);
    init(node, parent, numBureau);
}

/**
 * Initialise les textes
 * @param node
 * @param parent
 */
export function initText(node, parent) {
    let numBureau = node.textContent;
    init(node, parent, numBureau);
}

/**
 * Action lorsque l'on passe la souris sur un bureau
 * Affiche le nom de l'équipe et le taux d'occupation
 * @param event
 * @param equipe
 * @param taux
 */
function hoverAction(event, equipe, taux) {
    let divHover = document.createElement("div");
    divHover.innerHTML = `${equipe} <br> ${taux}`;

    // Prend les coordonnées de la souris
    divHover.style.left = event.clientX + 20 + "px";
    divHover.style.top = event.clientY - 80 + "px";
    divHover.classList.add("hover");

    document.body.appendChild(divHover);
}

/**
 * Action lorsque l'on sort de la zone d'un bureau ou du svg
 */
function outAction() {
    let divHovers = document.getElementsByClassName("hover");
    for (let divHover of divHovers) {
        divHover.remove();
    }
}