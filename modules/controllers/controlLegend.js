"use strict";
import * as displayLegend from "../displays/displayLegend.js";

export let legendShow = false;
let click = 0;
export function init() {
    let legend = document.getElementById("legende");
    let legendButton = document.getElementById("button__legende");

    legendButton.addEventListener("click", function () {
        click++;
        if (click % 2 === 0) {
            legendShow = false;
            legendButton.innerHTML = "légende";
            legend.style.left = "-100%";
        } else {
            legendShow = true;
            legendButton.innerHTML = "Carte";
            legend.style.left = "0";
            displayLegend.display();
        }
    });
}