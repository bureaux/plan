"use strict";
import * as model from "../model/model.js";
import * as displayNotification from "../displays/displayNotification.js";
import {admin} from "../model/model.js";

/**
 * Initialise lr drag and drop
 */
export function initPerson(personJson, node) {
    if (admin) {
        node.addEventListener("dragstart", function (event) {
            event.dataTransfer.setData("text1", JSON.stringify(personJson));
            event.dataTransfer.setData("text2", node.getAttribute("temp"));
            if (node.getAttribute("temp") === "true") {
                if (document.getElementById("previsualisationBureau")) document.getElementById("previsualisationBureau").style.display = "block";
            } else {
                if (document.getElementById("previsualisationTampon")) document.getElementById("previsualisationTampon").style.display = "block";
                event.dataTransfer.setData("text3", model.extractNumBureau(node));
            }
            node.style.opacity = "0.5";
        });

        node.addEventListener("dragend", function () {
            if (document.getElementById("previsualisationBureau")) document.getElementById("previsualisationBureau").style.display = "none";
            if (document.getElementById("previsualisationTampon")) document.getElementById("previsualisationTampon").style.display = "none";
            node.style.opacity = "1";
        });
    }

}

export function initBureau() {
    if (admin) {
        let desk = document.getElementById("desk_values");
        let tampon = document.getElementById("tampon");

        desk.addEventListener("dragover", function (event) {
            event.preventDefault();
        });

        desk.addEventListener("drop", async function (event) {
                if (!desk.classList.contains("non-repertorie")) {
                    event.preventDefault();
                    event.stopPropagation();

                    let data = event.dataTransfer.getData("text1");
                    let person = JSON.parse(data);

                    if (event.dataTransfer.getData("text2") === "true") {
                        let list = model.jsonDatas[model.jsonDatas.length - 1][0].personnels;
                        list.forEach(p => {
                            if (p.nom === person.nom && p.prenom === person.prenom) {
                                list.splice(list.indexOf(p), 1);
                            }
                        })
                        model.setListTemp(list);

                        model.jsonDatas.forEach(batiment => {
                            batiment.forEach(bureau => {
                                if (bureau.libelle === model.currentOffice) {
                                    bureau.personnels.push(person);
                                    displayNotification.displaySuccess(person.nom + " " + person.prenom + " a été ajouté au bureau " + bureau.libelle);
                                    model.addLogs("Ajout de " + person.nom + " " + person.prenom + " au bureau " + bureau.libelle);
                                }
                            });
                        });

                        await model.updateViews();
                    }
                } else {
                    displayNotification.displayError("Vous ne pouvez pas ajouter de personne dans un bureau non répertorié");
                }
            });

        tampon.addEventListener("dragover", function (event) {
            event.preventDefault();
        });

        tampon.addEventListener("drop", function (event) {
            event.preventDefault();
            event.stopPropagation();

            let data = event.dataTransfer.getData("text1");
            let person = JSON.parse(data);

            data = event.dataTransfer.getData("text3");
            let bureau = data;

            if (event.dataTransfer.getData("text2") === "false") {
                model.cut(person, bureau);
            }
        });

    }
}