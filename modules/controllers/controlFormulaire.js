"use strict";
import * as model from "../model/model.js";
import * as viewSvg from "../views/viewSvg.js";
import * as viewInfos from "../views/viewInfos.js";
import * as displayNotification from "../displays/displayNotification.js";

// import * as svgExport from "../svg-export.min.js";


export function initAdd(numBureau, formulaire, flou) {
    let nom = document.getElementById("form__nom");
    let prenom = document.getElementById("form__prenom");
    let equipe = document.getElementById("form__equipe");
    let employeur = document.getElementById("form__employeur");
    let datedebut = document.getElementById("form__datedeb");
    let datefin = document.getElementById("form__datefin");
    let valider = document.getElementById("form__valider");
    let checkbox = document.getElementById("form__checkbox");

    let dateTemp = datefin.value;
    checkbox.addEventListener("click", function () {
        if (checkbox.checked) {
            datefin.value = "2099-12-31";
        } else {
            datefin.value = dateTemp;
        }
    });

    datefin.addEventListener("change", function () {
        checkbox.checked = datefin.value.split("-")[0] === "2099";
    });

    let datas = model.jsonDatas;
    valider.addEventListener("click", async function () {
        // remet dans l'odre les dates
        let datedebutValue = datedebut.value.split("-")[2] + "-" + datedebut.value.split("-")[1] + "-" + datedebut.value.split("-")[0];
        let datefinValue = datefin.value.split("-")[2] + "-" + datefin.value.split("-")[1] + "-" + datefin.value.split("-")[0];

        let data = {
            "nom": nom.value,
            "prenom": prenom.value,
            "equipe": equipe.value.toUpperCase(),
            "employeur": employeur.value.toUpperCase(),
            "telephone": null,
            "date_debut": datedebutValue,
            "date_fin": datefinValue
        };
        datas.forEach(batiment => {
            batiment.forEach(bureau => {
                if (bureau.libelle === numBureau) {
                    bureau.personnels.push(data);
                }
            })
        })
        flou.style.display = "none";
        formulaire.innerHTML = "";
        await viewSvg.update();
        await viewInfos.update();
        model.save();
        displayNotification.displaySuccess(nom.value + " " + prenom.value + " a été ajouté au bureau " + numBureau);
        model.addLogs("Ajout de " + nom.value + " " + prenom.value + " au bureau " + numBureau);
    });
}

export function initEdit(person, numBureau, formulaire, flou) {
    let nom = document.getElementById("form__nom");
    let prenom = document.getElementById("form__prenom");
    let equipe = document.getElementById("form__equipe");
    let employeur = document.getElementById("form__employeur");
    let taux = document.getElementById("form__taux");
    let valider = document.getElementById("form__valider");
    let supprimer = document.getElementById("form__supprimer");
    let datedebut = document.getElementById("form__datedeb");
    let datefin = document.getElementById("form__datefin");
    let checkbox = document.getElementById("form__checkbox");

    if (datefin.value.split("-")[0] === "2099") {
        checkbox.checked = true;
    }

    let dateTemp = datefin.value;
    checkbox.addEventListener("click", function () {
        if (checkbox.checked) {
            datefin.value = "2099-12-31";
            taux.value = 1.00;
        } else {
            datefin.value = dateTemp;
        }
    });

    datefin.addEventListener("change", function () {
        checkbox.checked = datefin.value.split("-")[0] === "2099";
    });

    let datas = model.jsonDatas;
    valider.addEventListener("click", async function () {
        // remet dans l'odre les dates
        let datedebutValue = datedebut.value.split("-")[2] + "-" + datedebut.value.split("-")[1] + "-" + datedebut.value.split("-")[0];
        let datefinValue = datefin.value.split("-")[2] + "-" + datefin.value.split("-")[1] + "-" + datefin.value.split("-")[0];

        datas.forEach(batiment => {
            batiment.forEach(bureau => {
                bureau.personnels.forEach(pers => {
                    if (pers.nom === person.nom && pers.prenom === person.prenom) {
                        pers.nom = nom.value;
                        pers.prenom = prenom.value;
                        pers.equipe = equipe.value.toUpperCase();
                        pers.employeur = employeur.value.toUpperCase();
                        pers.taux_occupation = taux.value;
                        pers.date_debut = datedebutValue
                        pers.date_fin = datefinValue;
                    }
                })
            })
        })
        flou.style.display = "none";
        formulaire.innerHTML = "";
        await viewSvg.update();
        await viewInfos.update();
        model.save();
        displayNotification.displaySuccess(person.nom + " " + person.prenom + " a été modifié");
    });

    supprimer.addEventListener("click", async function () {
        await model.deletePerson(person, numBureau);
        flou.style.display = "none";
        formulaire.innerHTML = "";
        model.save();
    });
}

export function initExportData(formulaire, flou) {
    document.getElementById("form__valider").addEventListener("click", function () {
        let jsonString = JSON.stringify(model.jsonDatas);
        let blob = new Blob([jsonString], {type: "application/json"});
        let url = URL.createObjectURL(blob);

        let a = document.createElement("a");
        a.href = url;
        a.download = `${document.getElementById("form__name").value}.json`;
        a.title = document.getElementById("form__name").value;

        document.body.appendChild(a);
        a.click();

        URL.revokeObjectURL(url);
        document.body.removeChild(a);

        flou.style.display = "none";
        formulaire.innerHTML = "";
        displayNotification.displaySuccess("Le fichier " + a.download + " a été exporté avec succès");
    });
}

export function initExportSvg(formulaire, flou) {
    document.getElementById("form__valider").addEventListener("click", function () {
        let svg = document.getElementById("svg__" + model.currentMap).getElementsByTagName("svg")[0];
        let nom = document.getElementById("form__name").value;

        // Cloner le SVG pour éviter de modifier l'original
        svg = svg.cloneNode(true);

        // Nettoyer et préparer le SVG
        Array.from(svg.getElementsByTagName("path")).forEach(node => {
            cleanSvg(node)
        });

        Array.from(svg.getElementsByTagName("rect")).forEach(node => {
            cleanSvg(node);
        });

        Array.from(svg.getElementsByTagName("ellipse")).forEach(node => {
            node.remove();
        });

        // Obtenir les dimensions du SVG
        const svgWidth = svg.viewBox.baseVal.width * 2;
        const svgHeight = svg.viewBox.baseVal.height * 2;

        // Créer un canvas avec fond blanc
        const canvas = document.createElement("canvas");
        canvas.width = svgWidth;
        canvas.height = svgHeight;
        const context = canvas.getContext("2d");

        // Remplir le canvas avec fond blanc
        context.fillStyle = "white";
        context.fillRect(0, 0, svgWidth, svgHeight);

        // Dessiner le SVG sur le canvas
        const svgXml = new XMLSerializer().serializeToString(svg);
        const img = new Image();
        img.src = "data:image/svg+xml;base64," + btoa(svgXml);

        img.onload = function () {
            console.log("onload");
            // Dessiner l'image SVG sur le canvas (par-dessus le fond blanc)
            context.drawImage(img, 0, 0, svgWidth, svgHeight);

            // Convertir le canvas en image PNG et télécharger
            const pngUrl = canvas.toDataURL("image/png");

            let a = document.createElement("a");
            a.href = pngUrl;
            a.download = `${nom}.png`;

            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);

            displayNotification.displaySuccess(`Le fichier ${nom}.png a été exporté avec succès`);
        };

        flou.style.display = "none";
        formulaire.innerHTML = "";
    });
}


export function initEditOffice(bureau, formulaire, flou) {
    let tauxPerm = document.getElementById("form__tauxPerm");
    let tauxNonPerm = document.getElementById("form__tauxNonPerm");
    let valider = document.getElementById("form__valider");

    let datas = model.jsonDatas;
    valider.addEventListener("click", async function () {
        datas.forEach(batiment => {
            batiment.forEach(b => {
                if (bureau.libelle === b.libelle) {
                    bureau.taux_perm = parseFloat(tauxPerm.value);
                    bureau.taux_non_perm = parseFloat(tauxNonPerm.value);
                    console.log(bureau.taux_non_perm);
                }
            })
        })
        flou.style.display = "none";
        formulaire.innerHTML = "";
        await viewSvg.update();
        await viewInfos.update();
        model.save();
        displayNotification.displaySuccess("Les taux du bureau " + bureau.libelle + " ont été modifiés");
    });
}

export function initTauxGlobal(formulaire, flou) {
    let tauxPermNode = document.getElementById("form__tauxPerm");
    let tauxNonPermNode = document.getElementById("form__tauxNonPerm");
    let valider = document.getElementById("form__valider");

    valider.addEventListener("click", async function () {
        model.setTaux(tauxPermNode.value, tauxNonPermNode.value);
        flou.style.display = "none";
        formulaire.innerHTML = "";
        await viewSvg.update();
        await viewInfos.update();
        model.save();
        displayNotification.displaySuccess("Les taux globaux ont été modifiés");
    });
}

export function initJson2Csv(formulaire, flou) {
    let valider = document.getElementById("form__valider");

    let jsonFinal = [{"libelle": "", "nombre_place": "", "etage": "", "personnels": [{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""}, {"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""}, {"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""}, {"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""}, {"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""},{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""},{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""},{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""},{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""},{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""},{"nom": "", "prenom": "", "telephone": "", "date_debut": "", "date_fin": "", "equipe": "", "employeur": "", "inria_login": ""}]},];
    for (let i = 0; i < model.jsonDatas.length-2; i++) {
        let jsonCopy = JSON.parse(JSON.stringify(model.jsonDatas[i]));
        jsonCopy.forEach(bureau => {
            delete bureau.commentaire;
            delete bureau.date_commentaire;
            jsonFinal.push(bureau);
        });
    }

    valider.addEventListener("click", function () {
        let nom = document.getElementById("form__name").value;
        let csv = jsonToCsv(jsonFinal);

        //download the csv file
        const blob = new Blob([csv], {type: 'text/csv'});
        const url = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = url;
        a.download = `${nom}.csv`;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
        URL.revokeObjectURL(url);

        flou.style.display = "none";
        formulaire.innerHTML = "";
    });
}

function jsonToCsv(jsonData) {
    // Check if jsonData is an array and has at least one object
    if (!Array.isArray(jsonData)) {
        jsonData = [jsonData];
    }

    // Initialize variables
    let csvContent = "";

    // Check if the array is empty
    if (jsonData.length === 0) {
        throw new Error('Invalid JSON data. The array is empty.');
    }

    // Function to flatten nested objects
    function flattenObject(obj, parentKey = '') {
        let result = {};
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                let newKey = parentKey ? `${parentKey}.${key}` : key;
                if (typeof obj[key] === 'object' && obj[key] !== null) {
                    // Recursively flatten nested objects
                    Object.assign(result, flattenObject(obj[key], newKey));
                } else {
                    result[newKey] = obj[key];
                }
            }
        }
        return result;
    }

    // Flatten the first object in the array to get header
    const flattenedHeader = flattenObject(jsonData[0]);
    const header = Object.keys(flattenedHeader);
    csvContent += header.join(",") + "\n";

    // Iterate through each object in the array
    jsonData.forEach((item) => {
        const flattenedItem = flattenObject(item);
        // Extract values in the same order as the header
        const row = header.map((key) => flattenedItem[key]);

        // Convert values to CSV format and append to the content
        csvContent += row.join(",") + "\n";
    });

    return csvContent;
}

function cleanSvg(node) {
    node.style.stroke = "black";
    if (node.style.fill === "") {
        node.style.fill = "white";
    }
}