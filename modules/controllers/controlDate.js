"use strict";
import * as model from "../model/model.js";

/**
 * Initialise le choix de l'année
 */
export function init() {
    if (model.admin) {
        let headerRight = document.getElementById("header-right");
        headerRight.innerHTML = `
    <div class="date__container" id="date-container">
            <input id="slide" type="range">
            <div class="date">
                <button id="date__moins">-</button>
                <p id="slide-output"></p>
                <button id="date__plus">+</button>
            </div>
        </div>
    `;

        let currentYear = new Date().getFullYear();

        let modelMonth = parseInt(model.month);
        let modelYear = parseInt(model.year);

        let slider = document.getElementById("slide");
        let output = document.getElementById("slide-output");
        let plus = document.getElementById("date__plus");
        let moins = document.getElementById("date__moins");

        slider.min = 1;
        slider.max = 24;

        if (modelYear > currentYear) {
            slider.value = modelMonth + 12;
        } else {
            slider.value = modelMonth;
        }

        output.textContent = modelMonth + "/" + modelYear;
        if (modelMonth < 10) {
            output.textContent = "0" + output.textContent;
        }

        slider.addEventListener("change", function () {
            if (slider.value > 12) {
                output.textContent = (slider.value - 12) + "/" + (currentYear + 1);
            } else {
                output.textContent = slider.value + "/" + currentYear;
            }
            if (output.textContent.split("/")[0] < 10) {
                output.textContent = "0" + output.textContent;
            }
            model.setDate(output.textContent.split("/")[1], output.textContent.split("/")[0]);
        });

        plus.addEventListener("click", function () {
            if (slider.value < 24) {
                slider.value++;
                if (slider.value > 12) {
                    output.textContent = (slider.value - 12) + "/" + (currentYear + 1);
                } else {
                    output.textContent = slider.value + "/" + currentYear;
                }
                if (output.textContent.split("/")[0] < 10) {
                    output.textContent = "0" + output.textContent;
                }
                model.setDate(output.textContent.split("/")[1], output.textContent.split("/")[0]);
            }
        });

        moins.addEventListener("click", function () {
            if (slider.value > 1) {
                slider.value--;
                if (slider.value > 12) {
                    output.textContent = (slider.value - 12) + "/" + (currentYear + 1);
                } else {
                    output.textContent = slider.value + "/" + currentYear;
                }
                if (output.textContent.split("/")[0] < 10) {
                    output.textContent = "0" + output.textContent;
                }
                model.setDate(output.textContent.split("/")[1], output.textContent.split("/")[0]);
            }
        });
    }
}