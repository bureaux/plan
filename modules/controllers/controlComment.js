"use strict";
import * as model from "../model/model.js"
import * as displayNotification from "../displays/displayNotification.js";

/**
 * Initialise le bouton save des commentaires
 */
export function init() {
    if (model.admin) {
	let node = document.getElementById("comment");
        let saveComments = document.getElementById("button-save-commentaire");
	
        saveComments.addEventListener("click", function () {
            model.jsonDatas.forEach(batiment => {
		batiment.forEach(bureau => {
                    if (bureau.libelle === model.currentOffice) {
			var today = new Date();
			var yyyy = today.getFullYear();
			var mm = today.getMonth() + 1; // Months start at 0!
			var dd = today.getDate();
			if (dd < 10) dd = '0' + dd;
			if (mm < 10) mm = '0' + mm;
			var now = {};
			now.date = dd + '/' + mm + '/' + yyyy;
			now.timezone_type  = 3;
			now.timezone = "UTC";
			bureau.date_commentaire = now;
			if (bureau.commentaire)
			    bureau.commentaire += node.value;
			else
			    bureau.commentaire  = node.value;
		        displayNotification.displaySuccess("Commentaire mis à jour");
			model.addLogs("Mise à jour du commentaire \"" + node.value + "\" dans le bureau " + bureau.libelle);
                    }
		});
            });
	});

    }
}
