"use strict";
import * as model from "../model/model.js"

/**
 * Initialise les boutons menus
 */
export function init() {
    if (model.admin) {
        let containerRight = document.getElementById("container-right");

        let containerCenter = document.getElementById("container-center");
        let showCenter = document.getElementById("button-show-tampon");
        let closeCenter = document.getElementById("button-close-tampon");

        closeCenter.addEventListener("click", function () {
            containerCenter.style.display = "none";
            showCenter.style.display = "block";
            containerRight.style.width = "50%";
        });

        showCenter.addEventListener("click", function () {
            containerCenter.style.display = "block";
            showCenter.style.display = "none";
            containerRight.style.width = "40%";
        });

        let deskValues = document.getElementById("desk_values");

        let footer = document.getElementById("footer");
        let showFooter = document.getElementById("button-show-commentaire");
        let closeFooter = document.getElementById("button-close-commentaire");

        closeFooter.addEventListener("click", function () {
            footer.style.display = "none";
            showFooter.style.display = "block";
            deskValues.style.height = "90%";
        });

        showFooter.addEventListener("click", function () {
            footer.style.display = "block";
            showFooter.style.display = "none";
            deskValues.style.height = "70%";
        });

        deskValues.style.height = "70%";
        containerRight.style.width = "40%";
    } else {
        let containerRight = document.getElementById("container-right");
        let deskValues = document.getElementById("desk_values");
        deskValues.style.height = "100%";
        containerRight.style.width = "50%";
    }
}