"use strict";
import * as model from "../model/model.js";
import * as viewSvg from "../views/viewSvg.js";

/**
 * Initialise le choix du batiment
 */
export async function init() {
    let batiments = document.getElementById("batiments");
    batiments.innerHTML = "<h2>Batiment</h2>";
    const reponse = await fetch("../../conf.txt");
    const data = await reponse.text();
    let tab = data.split("\n");
    tab.forEach(line => {
        if (line.includes("=") && !line.includes("#")) {
            let word = line.split("=")[0];
            if (word === "batiment") {
                let key = line.split("=")[1]
                batiments.innerHTML +=
                    `
                    <input class="radio-input" name="radio-group" type="radio" id="radio-building-${key}">
                    <label class="radio-label" for="radio-building-${key}" id="building-${key}">
                        <span class="radio-inner-circle"></span>
                        ${key.toUpperCase()}
                    </label>
                    `;
            }
        }
    });
    batiments.addEventListener("click", async function (event) {
        let target = event.target;
        if (target.tagName === "LABEL") {
            let key = target.id.split("-")[1];
            model.setBuilding(key);
            await model.checkBuilding();
            await viewSvg.update();
        } else if (target.tagName === "SPAN") {
            let key = target.parentElement.id.split("-")[1];
            model.setBuilding(key);
            await model.checkBuilding();
            await viewSvg.update();
        }
    });
    document.getElementById(document.getElementById("building-" + model.building).htmlFor).checked = true;
}
