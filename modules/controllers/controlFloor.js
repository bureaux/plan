"use strict";
import * as model from "../model/model.js";
import * as viewSvg from "../views/viewSvg.js";

/**
 * Initialise le choix de l'étage

 */
export async function init() {
    let nombreEtages = 0;

    let etages = document.getElementById("floors");
    etages.innerHTML = "<h2>Etage</h2>";
    const reponse = await fetch("../../conf.txt");
    const data = await reponse.text();
    let tab = data.split("\n");
    tab.forEach(line => {
        if (line.includes("=") && !line.includes("#")) {
            let word = line.split("=")[0];
            if (word === "batiment") {
                let value = line.split("=")[2];
                if (value > nombreEtages) {
                    nombreEtages = value;
                }
            }
        }
    });

    for (let i = 0; i < nombreEtages; i++) {
        etages.innerHTML +=
            `
            <input class="radio-input" name="radio-group" type="radio" id="radio-floor-${i}">
                <label class="radio-label" for="radio-floor-${i}" id="floor-${i}">
                    <span class="radio-inner-circle"></span>
                    ${i}
                </label>
            `;
    }

    etages.addEventListener("click", async function (event) {
        let target = event.target;
        if (target.tagName === "LABEL") {
            let key = target.id.split("-")[1];
            model.setFloor(key);
            await viewSvg.update();
        } else if (target.tagName === "SPAN") {
            let key = target.parentElement.id.split("-")[1];
            model.setFloor(key);
            await viewSvg.update();
        }
    });
    document.getElementById(document.getElementById("floor-" + model.floor).htmlFor).checked = true;
}