"use strict";
import * as model from "../model/model.js";

export async function init() {
    return new Promise((resolve) => {
        if (model.admin) {
            let container = document.getElementById("container-right");
            let footer = document.createElement("div");
            footer.className = "footer";
            footer.id ="footer";
            footer.innerHTML = `
            <button class="button-top-right" id="button__commentaire">commentaire</button>
            <button class="button-top-left" id="button-close-commentaire">-</button>
            <button class="button-bottom-right" id="button-save-commentaire">sauvegarder</button>
            <textarea id="comment" name="comment" rows="4" cols="50"></textarea>
            `;
            container.appendChild(footer);
        }
        resolve();
    });
}

/**
 * Met à jour la vue
 */
export async function update() {
    return new Promise((resolve) => {
        if (model.admin) {
            let node = document.getElementById("comment");
            let bureau = model.findOfficeJson(model.currentOffice);
            node.value = "";
            if (bureau !== undefined) {
                if (bureau.date_commentaire && bureau.commentaire !== null) node.value = "[" + bureau.date_commentaire.date.split(" ")[0] + "]\n\n";
                if (bureau.commentaire) node.value += bureau.commentaire;
            }	    
        }
        resolve();
    });
}
