"use srict";
import * as model from "../model/model.js";
import * as displayPerson from "../displays/displayPerson.js";
import * as displayPervisualisation from "../displays/displayPrevisualisation.js";
import * as displayFormulaire from "../displays/displayFormulaire.js";


/**
 * Met à jour la vue
 */
export async function update(qualifiedName, value) {
    return new Promise(async (resolve) => {
            if (model.admin) {
                // div principale
                let div = document.getElementsByClassName("desk_values").item(0);
                div.innerHTML = "";

                // Récupération du bureau courant
                let numBureau = model.currentOffice;

                // Préparation des données bureau json + son équipe
                let bureau = model.findOfficeJson(numBureau);
                let equipe = model.getEquipe(bureau);
                if (equipe instanceof Array) equipe = "Equipe Mixte";

                // Si le bureau existe dans le json
                if (bureau) {
                    // Création de la div pour les personnels
                    let div2 = document.createElement("div");
                    div2.className = "desk_values_team"
                    div2.id = "desk_values_team";
                    let nbPerson = model.getTauxBureau(bureau);
                    displayPerson.displayTeam(div2, "bureau", numBureau)
                        .then(async () => {
                            // Prévisualisation du drag and drop
                            displayPervisualisation.display(div2, "previsualisationBureau");

                            // Bouton pour ajouter une personne
                            let addButton = document.createElement("div");
                            addButton.className = "person";
                            addButton.classList.add("card__button");
                            addButton.id = "card__add";
                            addButton.textContent = "+ Ajouter";
                            addButton.addEventListener("click", function () {
                                displayFormulaire.displayAdd(numBureau);
                            });
                            div2.appendChild(addButton);

                            // Récupération de la superficie
                            let superficie = await model.getSuperficie(numBureau);

                            // Création du header avec les informations du bureau
                            let buttonEdit = document.createElement("button");
                            buttonEdit.id = "button_desk_edit";
                            buttonEdit.className = "button-bottom-right";
                            buttonEdit.innerHTML = `edit`;
                            buttonEdit.addEventListener("click", function () {
                                displayFormulaire.displayEditOffice(bureau);
                            });

                            let buttonImprimer = document.createElement("button");
                            buttonImprimer.id = "button_desk_imprimer";
                            buttonImprimer.className = "button-bottom-left";
                            buttonImprimer.innerHTML = `etiquette`;
                            buttonImprimer.addEventListener("click", function () {
                               fetch(`../../datas/etiquettes/${numBureau}.pdf`)
                                   .then(response => response.blob())
                                      .then(blob => {
                                        const url = window.URL.createObjectURL(new Blob([blob]));
                                        const link = document.createElement('a');
                                        link.href = url;
                                        link.setAttribute('download', `${numBureau}.pdf`);
                                        document.body.appendChild(link);
                                        link.click();
                                        link.remove();
                                      });
                            });

                            let divInfos = document.createElement("div");
                            divInfos.className = "desk_values_info";
                            divInfos.innerHTML = `
                            <div><h2>Bureau</h2><span>${bureau.libelle}</span></div>
                            <div><h2>Equipe</h2><span>${equipe}</span></div>
                            <div><h2>Places</h2><span>${bureau.nombre_place}</span></div>
                            <div><h2>Libre</h2><span>${(bureau.nombre_place - nbPerson).toFixed(1)}</span></div>
                            <div><h2>Superficie</h2><span>${superficie}</span></div>`;

                            div.appendChild(buttonEdit);
                            div.appendChild(buttonImprimer);
                            div.appendChild(divInfos);
                            div.appendChild(div2);
                            div.classList.remove("non-repertorie");
                        });
                } else if (numBureau === "info-accueil") {
                    div.classList.add("non-repertorie");
                    div.innerHTML = "Veuillez sélectionner un bureau";
                } else {
                    div.classList.add("non-repertorie");
                    div.innerHTML = "Salle non répertoriée dans la base de données";
                }

                // Bouton titre en bleu (bureau)
                let titre = document.createElement("button");
                titre.classList.add("button-top-right");
                titre.innerHTML = "bureau";
                div.appendChild(titre);

            } else {
                // div principale
                let div = document.getElementsByClassName("desk_values").item(0);
                div.innerHTML = "";

                // Récupération du bureau courant
                let numBureau = model.currentOffice;

                // Préparation des données bureau json + son équipe
                let bureau = model.findOfficeJson(numBureau);
                let equipe = model.getEquipe(bureau);
                if (equipe instanceof Array) equipe = "Equipe Mixte";

                // Si le bureau existe dans le json
                if (bureau) {
                    // Création de la div pour les personnels
                    let div2 = document.createElement("div");
                    div2.className = "desk_values_team"
                    div2.id = "desk_values_team";
                    bureau.personnels.forEach(personnel => {
                        displayPerson.display(personnel, div2, "bureau", numBureau);
                    });

                    // Récupération de la superficie
                    let superficie = await model.getSuperficie(numBureau);

                    // Création du header avec les informations du bureau
                    let divInfos = document.createElement("div");
                    divInfos.className = "desk_values_info";
                    divInfos.innerHTML = `
                <div><h2>Bureau</h2><span>${bureau.libelle}</span></div>
                <div><h2>Equipe</h2><span>${equipe}</span></div>
                <div><h2>Places</h2><span>${bureau.nombre_place}</span></div>
                <div><h2>Superficie</h2><span>${superficie}</span></div>
                `;

                    div.appendChild(divInfos);
                    div.appendChild(div2);
                    div.classList.remove("non-repertorie");
                } else if (numBureau === "info-accueil") {
                    div.classList.add("non-repertorie");
                    div.innerHTML = "Veuillez sélectionner un bureau";
                } else {
                    div.classList.add("non-repertorie");
                    div.innerHTML = "Salle non répertoriée dans la base de données";
                }

                // Bouton titre en bleu (bureau)
                let titre = document.createElement("button");
                titre.classList.add("button-top-right");
                titre.innerHTML = "bureau";
                div.appendChild(titre);

                let buttonImprimer = document.createElement("button");
                buttonImprimer.id = "button_desk_imprimer";
                buttonImprimer.className = "button-bottom-left";
                buttonImprimer.innerHTML = `etiquette`;
                buttonImprimer.addEventListener("click", function () {
                    fetch(`../../datas/etiquettes/${numBureau}.pdf`)
                        .then(response => response.blob())
                        .then(blob => {
                            const url = window.URL.createObjectURL(new Blob([blob]));
                            const link = document.createElement('a');
                            link.href = url;
                            link.setAttribute('download', `${numBureau}.pdf`);
                            document.body.appendChild(link);
                            link.click();
                            link.remove();
                        });
                });
                div.appendChild(buttonImprimer);
            }

            // Fin de la promesse
            resolve();
        });
}
