"use strict";
import * as model from "../model/model.js";
import * as displayPerson from "../displays/displayPerson.js";
import * as displayPervisualisation from "../displays/displayPrevisualisation.js";

export async function init() {
    let container = document.getElementById("container-center");
    if (model.admin) {
        container.innerHTML = `
            <div class="tampon" id="tampon">
               <button class="button-top-right" id="button__tampon">tampon</button>
               <button class="button-top-left" id="button-close-tampon">-</button>
               <div class="tampon__content"></div>
            </div>
            `;
    }
}

/**
 * Met à jour la vue
 */
export async function update() {
    return new Promise((resolve) => {
        if (model.admin) {
            let tampon = document.getElementsByClassName("tampon__content").item(0);
            tampon.innerHTML = "";
            model.jsonDatas[model.jsonDatas.length - 1][0].personnels.forEach(pers => {
                displayPerson.display(pers, tampon, "tampon");
            });
            displayPervisualisation.display(tampon, "previsualisationTampon");
        }
        resolve();
    });
}