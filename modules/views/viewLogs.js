"use strict";
import * as model from "../model/model.js"
import * as displayFormulaire from "../displays/displayFormulaire.js"

/**
 * Met à jour la vue
 */
export async function update() {
    return new Promise((resolve) => {
        let logs = model.logs;

        // Gestion des formulaire
        let formulaire = document.getElementById("formulaire");
        formulaire.innerHTML = `
        <form class="form">
            <p class="title">Logs</p>
            <p class="message">Voici toutes les actions qui ont été effectuées depuis le début de votre simulation</p>
            <div class="form__content"></div>
        </form>
        `;

        logs.forEach(log => {
            let div = document.createElement("div");
            div.innerHTML = `<p>${log}</p>`;
            formulaire.getElementsByClassName("form__content")[0].appendChild(div);
        });

        // Gestion du flou et du close
        let flou = displayFormulaire.flouInit();
        let close = displayFormulaire.closeInit(formulaire, flou);
        formulaire.appendChild(close);
        resolve();
    });
}
