"use strict";
import * as model from "../model/model.js";
import {legendShow} from "../controllers/controlLegend.js";
import * as displayLegend from "../displays/displayLegend.js";
import * as viewMap from "./viewMap.js";
import * as controlMap from "../controllers/controlMap.js";

/**
 * Initialise la vue
 */
export async function init() {
    return new Promise(resolve => {
        let map = document.getElementById("map__loria");
        fetch("./../../svg/" + model.building + model.floor + ".svg")
            .then(response => {
                return response.text();
            })
            .then(data => {
                map.innerHTML = `<div class="div__map">${data}</div>`;
                map.lastChild.id = "svg__0";
                if (model.admin) {
                    map.lastChild.style.opacity = "1";
                    map.lastChild.style.border = "2px dashed black";
                }

                let mode = document.createAttribute("mode");
                mode.value = model.mode;
                map.lastChild.setAttributeNode(mode);
                let floor = document.createAttribute("floor");
                floor.value = model.floor;
                map.lastChild.setAttributeNode(floor);
                let building = document.createAttribute("building");
                building.value = model.building;
                map.lastChild.setAttributeNode(building);

                controlMap.init(map.lastChild);
            })
            .then(() => {
                // Si la légende est activée, on l'affiche
                if (legendShow) displayLegend.display();

                // Mise à jour des statistiques
                viewMap.update();

                // Promesse résolue
                resolve();
            })
            .catch(error => {
                console.error(error);
            });
    });
}


/**
 * Met à jour la vue
 */
export async function update() {
    return new Promise((resolve, reject) => {
        let map = document.getElementById("map__loria");
        let currentSvg = document.getElementById("svg__" + model.currentMap);

        if (!map || !currentSvg) {
            reject(new Error("Éléments introuvables"));
            return;
        }

        fetch("./../../svg/" + model.building + model.floor + ".svg")
            .then(response => {
                if (!response.ok) {
                    throw new Error("Échec de récupération du SVG");
                }
                return response.text();
            })
            .then(data => {


                let newSvg = document.createElement("div");
                newSvg.className = "div__map";
                newSvg.innerHTML = data;

                if (currentSvg.getElementsByTagName("button").length > 0) {
                    newSvg.appendChild(currentSvg.getElementsByTagName("button")[0]);
                }

                    // Remplace le SVG actuel par le nouveau SVG
                map.replaceChild(newSvg, currentSvg);
                newSvg.id = "svg__" + model.currentMap;

                // Attributs du nouveau SVG
                let mode = document.createAttribute("mode");
                mode.value = model.mode;
                newSvg.setAttributeNode(mode);
                let floor = document.createAttribute("floor");
                floor.value = model.floor;
                newSvg.setAttributeNode(floor);
                let building = document.createAttribute("building");
                building.value = model.building;
                newSvg.setAttributeNode(building);

                // Initialise le contrôle de la carte pour le nouveau SVG
                controlMap.init(newSvg);

                // Style du nouveau SVG
                if (model.admin) {
                    newSvg.style.opacity = "1";
                    newSvg.style.border = "2px dashed black";
                }
                // Affiche la légende si activée
                if (legendShow) {
                    displayLegend.display();
                }

                // Met à jour les statistiques de la carte
                viewMap.update();

                // Promesse résolue
                resolve();
            })
            .catch(error => {
                reject(error);
            });
    });
}

