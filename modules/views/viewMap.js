"use strict";
import * as model from "../model/model.js";
import * as controlMap from "../controllers/controlMap.js";

let idSvg = 0;

/**
 * Met à jour la vue
 */
export function update() {
    return new Promise((resolve) => {
        if (model.admin) {
            // Récupération de l'élément stats
            let stats = document.getElementById("map__stats");
            stats.innerHTML = "";

            // Initialisation des variables
            let nbBureau = 0;
            let nbPersonne = 0;
            let etage = model.building.toUpperCase() + model.floor;

            // Parcours des bureaux + incrementation des variables
            model.jsonDatas.forEach(batiment => {
                batiment.forEach(bureau => {
                    if (bureau.libelle.includes(etage)) {
                        nbBureau++;
                        nbPersonne += bureau.personnels.length;
                    }
                });
            });

            // Affichage des stats
            stats.innerHTML = `
        <div class="stats">
            <p><span id="nb__bureau">${nbBureau}</span> bureaux</p>
        </div>
        <div class="stats">
            <p><span id="nb__person">${nbPersonne}</span> personnes</p>
        </div>
        <div class="stats">
                <p><span id="ratio">${(nbPersonne / nbBureau).toFixed(2)}</span> pers / bureau</p>
        </div>
        `;

            // Récupération de l'élément nomSimu
            let nomSimu = document.getElementById("name__simu");
            nomSimu.innerHTML = `<p>simulation : <span>${model.nameSimu}</span></p>`;

            // Bouton pour dupliquer la simulation
            let btnDupliquer = document.createElement("button");
            btnDupliquer.type = "button";
            btnDupliquer.id = "dupliquer";
            btnDupliquer.innerHTML = `<img src="./../../img/duplicate.png" alt="dupliquer">`;
            if (!document.getElementById("dupliquer")) document.getElementById("map").appendChild(btnDupliquer);
            btnDupliquer.addEventListener("click", function () {
                dupliquer();
            });
        }
        resolve();
    });
}

/**
 * Duplique la simulation
 */
function dupliquer() {
    idSvg++;

    // Récupération du dernier svg
    let map = document.getElementById("map__loria");
    let lastSvg = map.lastChild;

    // Ajout du svg
    map.appendChild(lastSvg.cloneNode(true));
    let node = map.lastChild;
    node.id = "svg__" + idSvg;
    node.style.opacity = "0.5";
    node.style.border = "2px dashed transparent";

    // Ajout du bouton de suppression
    let btnSupprimer = document.createElement("button");
    btnSupprimer.type = "button";
    btnSupprimer.className = "supprimer__map";
    btnSupprimer.innerHTML = `<img src="./../../img/delete.png" alt="supprimer" class="supprimer__map__img">`;
    node.appendChild(btnSupprimer);

    controlMap.init(node);
}