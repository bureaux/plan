"use strict";
import * as model from "../model/model.js";


export async function init() {
    return new Promise(resolve => {
        if (model.admin) {
            let body = document.body;
            let buttonMenu = document.createElement("button");
            buttonMenu.id = "option";
            buttonMenu.innerHTML = `<img src="../../img/settings.png" alt="bouton menu">`

            let menu = document.createElement("div");
            menu.id = "option__wrapper";
            menu.innerHTML = `
            <div id="option__img">
                <img id="current_mode_img" src="../../img/admin.png" alt="admin">
                <h1 id="current_mode">Simulation Mode</h1>
            </div>
            <ul>
                <li class="option__title">Données</li>
                <li class="option__simulation" id="json__save">Exporter en JSON</li>
                <li class="option__simulation" id="json2csv">Exporter en CSV</li>
                <li><input type="file" id="json__load" class="option__simulation json__load" accept="application/json"><label
                        for="file" class="option__simulation" id="json__import">Importer un JSON</label></li>
                <li class="option__simulation" id="json__reset">Reset les données</li>
                <li class="option__title">Correctifs</li>
                <li class="option__simulation" id="logs">Logs</li>
                <li class="option__simulation" id="correctif__import">Appliquer un correctif</li>
                <li class="option__title">Fonctionnalités</li>
                <li class="option__simulation" id="taux__global">Changer taux global</li>
                <li class="option__simulation" id="zone__nn">Zone non affecté</li>
                <li class="option__blue" id="a-propos">A propos</li>
            </ul>
            `;

            body.appendChild(buttonMenu);
            body.appendChild(menu);
        }
       resolve();
    });
}

