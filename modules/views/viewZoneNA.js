"use strict";
import * as model from "../model/model.js"
import * as displayFormulaire from "../displays/displayFormulaire.js"
import * as displayNotification from  "../displays/displayNotification.js";

/**
 * Met à jour la vue
 */
export async function update() {
    return new Promise((resolve) => {
        let zoneNA = model.jsonDatas[model.jsonDatas.length - 2][0].personnels;
        let zoneTampon = model.jsonDatas[model.jsonDatas.length - 1][0].personnels;

        // Gestion des formulaire
        let formulaire = document.getElementById("formulaire");
        formulaire.innerHTML = `
        <form class="form">
            <p class="title">Zone Non Affecté</p>
            <p class="message">Il est possible de faire revenir dans la zone tampon des personnes que vous avez auparavant supprimé</p>
            <div class="form__content"></div>
        </form>
        `;

        zoneNA.forEach(person => {
            let div = document.createElement("div");
            div.className = "card__zonena";
            div.innerHTML = `<p>${person.nom} ${person.prenom}</p>`;

            let divBtns = document.createElement("div");

            let btnReintegrer = document.createElement("button");
            btnReintegrer.type = "button";
            btnReintegrer.innerHTML = "réintégrer";
            btnReintegrer.className = "reintegrer";
            btnReintegrer.addEventListener("click", function () {
                zoneNA.forEach(p => {
                    if (person.nom === p.nom && person.prenom === p.prenom) {
                        zoneNA.splice(zoneNA.indexOf(p), 1);
                        zoneTampon.push(p);
                        model.updateViews();
                        displayNotification.displaySuccess(p.nom + " " + p.prenom + " a été réintégré dans la zone tampon");
                        model.addLogs("Réintégration de " + p.nom + " " + p.prenom + " dans la zone tampon");
                        update();
                    }
                })
            });

            let btnSupdef = document.createElement("button");
            btnSupdef.type = "button";
            btnSupdef.innerHTML = "sup définitivement"
            btnSupdef.className = "supprimer__def";
            btnSupdef.addEventListener("click", function () {
                zoneNA.forEach(p => {
                    if (person.nom === p.nom && person.prenom === p.prenom) {
                        zoneNA.splice(zoneNA.indexOf(p), 1);
                        model.updateViews();
                        displayNotification.displayWarning(p.nom + " " + p.prenom + " a été supprimé définitivement");
                        model.addLogs("Suppression définitive de " + p.nom + " " + p.prenom + " de la zone non affectée");
                        update();
                    }
                })
            });

            divBtns.appendChild(btnReintegrer);
            divBtns.appendChild(btnSupdef);
            div.appendChild(divBtns);
            formulaire.getElementsByClassName("form__content")[0].appendChild(div);
        });

        // Gestion du flou et du close
        let flou = displayFormulaire.flouInit();
        let close = displayFormulaire.closeInit(formulaire, flou);
        formulaire.appendChild(close);
        resolve();
    });
}
