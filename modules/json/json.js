"use strict";
import * as model from "../model/model.js";
import {displayError, displaySuccess} from "../displays/displayNotification.js";

let jsons = [];

export async function init() {
    return new Promise(async (resolve) => {
        await initWithUrl()
            .catch(async () => {
                displayError("Erreur de chargement des fichiers json, tentative de chargement local");
                await initWithoutUrl()
                    .catch(async () => {
                        displayError("Erreur de chargement des fichiers json en local")
                    });
            })
            .then(() => {
                displaySuccess("Chargement des fichiers json réussi");
            });
        resolve();
    });
}

export async function resetJson() {
    return new Promise(async (resolve, reject) => {
        jsons = [];
        let tampon = `
        [{
        "libelle":"tampon",
        "nombre_place":0,
        "etage":0,
        "personnels":[]
        }]`
        tampon = JSON.parse(tampon);

        let zonena = `
        [{
        "libelle":"zonena",
        "nombre_place":0,
        "etage":0,
        "personnels":[]
        }]`
        zonena = JSON.parse(zonena);

        let tab = [];
        init()
            .then(() => {
                jsons.forEach(json => {
                    tab.push(json);
                });
                tab.push(zonena);
                tab.push(tampon);
            })
            .then(() => {
                resolve(tab);
            })
            .catch(error => {
                reject(error);
            });
    });
}

async function initWithUrl() {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await fetch("../../conf.txt");
            const data = await response.text();
            let promises = [];
            let tab = data.split("\n");
            tab.forEach(line => {
                if (line.includes("=") && !line.includes("#")) {
                    let key = line.split("=")[0];
                    let value = line.split("=")[1];
                    if (model.admin && key === "simu") {
                        let promise = fetch(value)
                            .then(response => {
                                return response.json();
                            })
                            .then(json => {
                                jsons.push(json);
                            })
                        promises.push(promise);
                    } else if (!model.admin && key === "visu") {
                        let promise = fetch(value)
                            .then(response => {
                                return response.json();
                            })
                            .then(json => {
                                jsons.push(json);
                            })
                        promises.push(promise);
                    }
                }
            })
            await Promise.all(promises);
            resolve();
        } catch (error) {
            reject(error);
        }
    })
}


async function initWithoutUrl() {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await fetch("../../conf.txt")
            const data = await response.text();
            let tab = data.split("\n");
            let promises = [];
            tab.forEach(line => {
                if (line.includes("=") && !line.includes("#")) {
                    let key = line.split("=")[0];
                    let value = line.split("=")[1];
                    if (key === "localSimu" && model.admin) {
                        let promise = fetch(`../..${value}`)
                            .then(response => {
                                return response.json();
                            })
                            .then(json => {
                                jsons.push(json);
                            });
                        promises.push(promise);
                    } else if (key === "localVisu" && !model.admin) {
                        let promise = fetch(`../..${value}`)
                            .then(response => {
                                return response.json();
                            })
                            .then(json => {
                                jsons.push(json);
                            });
                        promises.push(promise);
                    }
                }
            });
            await Promise.all(promises);
            resolve();
        } catch (error) {
            reject(error);
        }
    });
}
