"use strict";
import * as controlFormulaire from "../controllers/controlFormulaire.js";
import * as model from "../model/model.js";


export function displayAdd(numBureau) {
    // Gestion des dates
    const date = new Date();
    let month = date.getMonth() + 1;
    let monthPlusOne = date.getMonth() + 2;
    if (month < 10) {
        month = "0" + month;
    }
    if (monthPlusOne < 10) {
        monthPlusOne = "0" + monthPlusOne;
    }
    let jour = date.getDate();
    if (jour < 10) {
        jour = "0" + jour;
    }
    let today = date.getFullYear() + "-" + month + "-" + jour;
    let todayPlusOne = date.getFullYear() + "-" + monthPlusOne + "-" + jour;

    // Gestions des formulaires
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Ajouter une personne </p>
    <p class="message">Veuillez rentrer les informations nécessaires pour ajouter une personne</p>
        <div class="flex">
        <label>
            <input required="" placeholder="" type="text" class="input" id="form__prenom">
            <span>Prénom</span>
        </label>

        <label>
            <input required="" placeholder="" type="text" class="input" id="form__nom">
            <span>Nom</span>
        </label>
        </div>  
            
    <label>
        <input required="" placeholder="" type="text" class="input" id="form__equipe">
        <span>Equipe</span>
    </label> 
        
    <label>
        <input required="" placeholder="" type="text" class="input" id="form__employeur">
        <span>Employeur</span>
    </label>
    <div id="div__checkbox"><label for="form__checkbox">Personne permanente : </label><input type="checkbox" id="form__checkbox"></div>
    <input class="date__picker" type="date" name="trip-start" value="${today}" min="${today}" max="${todayPlusOne}" id="form__datedeb"/>
    <input class="date__picker" type="date" name="trip-end" value="${todayPlusOne}" min="${todayPlusOne}" id="form__datefin"/>
    <button type="button" class="submit" id="form__valider">Valider</button>
    </form>
        `;

    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);
    controlFormulaire.initAdd(numBureau, formulaire, flou);
}

export function displayEditPerson(person, numBureau) {
    let taux;
    if (person.taux_occupation !== undefined) {
        taux = person.taux_occupation;
    } else if (person.date_fin.split("-")[2] === "2099") {
        taux = person.taux_occupation || model.tauxPerm || 1.00;
    } else {
        taux = person.taux_occupation || model.tauxNonPerm || 0.66;
    }

    // Gestion des dates de début et de fin de contrat
    let dateDeb = person.date_debut.split("-")[2] + "-" + person.date_debut.split("-")[1] + "-" + person.date_debut.split("-")[0];
    let dateFin = person.date_fin.split("-")[2] + "-" + person.date_fin.split("-")[1] + "-" + person.date_fin.split("-")[0];

    // Gestions du formulaire
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Modifier une personne </p>
    <p class="message">Il est possible de changer le nom, le prénom, l'équipe d'une personne et son taux d'occupation qui est par défaut de 1 pour une personne permanente et 0.66 sinon</p>
    <div class="flex">
        <label>
            <input required="" placeholder="" type="text" class="input" id="form__prenom" value="${person.prenom}">
            <span>Prénom</span>
        </label>

        <label>
            <input required="" placeholder="" type="text" class="input" id="form__nom" value="${person.nom}">
            <span>Nom</span>
        </label>
    </div> 
    <label>
        <input required="" placeholder="" type="number" step="0.01" class="input" id="form__taux" value="${taux}">
        <span>Taux d'occupation</span>
    </label>

    <label>
        <input required="" placeholder="" type="text" class="input" id="form__equipe" value="${person.equipe}">
        <span>Equipe</span>
    </label> 
    
    <label>
        <input required="" placeholder="" type="text" class="input" id="form__employeur" value="${person.employeur}">
        <span>Employeur</span>
    </label>
        
    <div id="div__checkbox"><label for="form__checkbox">Personne permanente : </label><input type="checkbox" id="form__checkbox"></div>
    <input class="date__picker" type="date" name="trip-start" value="${dateDeb}" id="form__datedeb"/>
    <input class="date__picker" type="date" name="trip-end" value="${dateFin}" id="form__datefin"/>
    
    <button type="button" class="submit" id="form__valider">Valider</button>
    <button type="button" class="submit__del" id="form__supprimer">Supprimer</button>
    </form>
        `;

    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);

    controlFormulaire.initEdit(person, numBureau, formulaire, flou);
}

export function displayEditOffice(bureau) {
    // Préparation des données
    let tauxPerm = bureau.taux_perm || model.tauxPerm;
    let tauxNonPerm = bureau.taux_non_perm || model.tauxNonPerm;

    // Gestions du formulaire
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Modifier les taux du bureau</p>
    <p class="message">Il est possible de changer les taux d'occupation de tout un bureau qui sont par défaut de 1 pour une personne permanente et 0.66 sinon</p>
    <label>
        <input required="" placeholder="" type="number" step="0.01" class="input" id="form__tauxPerm" value="${tauxPerm}">
        <span>Permanent</span>
    </label>

    <label>
        <input required="" placeholder="" type="number" step="0.01" class="input" id="form__tauxNonPerm" value="${tauxNonPerm}">
        <span>Non Permanent</span>
    </label> 
        
    <button type="button" class="submit" id="form__valider">Valider</button>
    </form>
        `;
    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);

    controlFormulaire.initEditOffice(bureau, formulaire, flou);
}

export function displayTauxGlobaux() {

    // Gestions du formulair
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Modifier les taux globaux</p>
    <p class="message">Il est possible de changer les taux d'occupation de tout les batiments qui sont par défaut de 1 pour une personne permanente et 0.66 sinon</p>
    <label>
        <input required="" placeholder="" type="number" step="0.01" class="input" id="form__tauxPerm" value="${model.tauxPerm}">
        <span>Permanent</span>
    </label>

    <label>
        <input required="" placeholder="" type="number" step="0.01" class="input" id="form__tauxNonPerm" value="${model.tauxNonPerm}">
        <span>Non Permanent</span>
    </label> 
        
    <button type="button" class="submit" id="form__valider">Valider</button>
    </form>
        `;

    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);

    controlFormulaire.initTauxGlobal(formulaire, flou);
}

export function displayExportData() {
    // Gestions du formulaire
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Exporter les données</p>
    <p class="message">Le nom de votre projet :</p>
    <label>
        <input required="" placeholder="" type="text" class="input" id="form__name">
        <span>Nom du projet</span>
    </label>
        <button class="submit" id="form__valider">Valider</button>
    </form>
        `;

    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);

    controlFormulaire.initExportData(formulaire, flou);
}

export function displayExportSvg() {
    // Gestions du formulaire
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Exporter l'image</p>
    <p class="message">Le nom de votre image :</p>
    <label>
        <input required="" placeholder="" type="text" class="input" id="form__name">
        <span>Nom de l'image</span>
    </label>
        <button type="button" class="submit" id="form__valider">Valider</button>
    </form>
        `;

    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);

    controlFormulaire.initExportSvg(formulaire, flou);
}

export function displayAPropos() {
    // Gestions du formulaire
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">A propos</p>
    <br>
    <div class="card__apropos">
    <p class="sub-title">Equipe du projet</p>
    <p class="message">Yannick Parmentier - Chef de projet</p>
    <p class="message">Jonathan Alcuta - Développeur</p>
    <p class="message">Axel Dung - Développeur</p>
    </div>
    
    <div class="card__apropos">
    <p class="sub-title">Sujet</p>
    <p class="message">Ce projet a été réalisé dans le cadre d'un stage de BUT Informatique qui a duré 10 semaines.</p>
    </div>
    </form>
    `;
    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);
}

export function displayJson2Csv() {
// Gestions du formulaire
    let formulaire = document.getElementById("formulaire");
    formulaire.innerHTML = `
      <form class="form">
    <p class="title">Exporter les données sous forme de CSV</p>
    <p class="message">Le nom de votre simulation :</p>
    <label>
        <input required="" placeholder="" type="text" class="input" id="form__name">
        <span>Nom de la simulation</span>
    </label>
        <button type="button" class="submit" id="form__valider">Valider</button>
    </form>
        `;

    // Gestion du flou et du close
    let flou = flouInit();
    let close = closeInit(formulaire, flou);
    formulaire.appendChild(close);
    controlFormulaire.initJson2Csv(formulaire, flou);
}

export function flouInit() {
    let flou = document.getElementById("flou");
    flou.style.display = "block";
    return flou;
}

export function closeInit(formulaire, flou) {
    let close = document.createElement("div");
    close.className = "close";
    close.addEventListener("click", function () {
        flou.style.display = "none";
        formulaire.innerHTML = "";
    });
    let img = document.createElement("img");
    img.src = "./../../img/close.png";
    img.className = "close__img";
    close.appendChild(img);
    return close;
}