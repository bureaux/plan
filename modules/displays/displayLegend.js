"use strict";
import * as model from "../model/model.js";


export function display() {
    let currentMap = document.getElementById("svg__" + model.currentMap);

    let legende = document.getElementById("legende");
    legende.innerHTML = "";

    switch (currentMap.getAttribute("mode")) {
        case "team":

            // on récupère toutes les équipes du svg actuel avec leur couleur
            let dico = new Map();
            let nodes = Array.from(currentMap.getElementsByTagName("path"));
            nodes.forEach(node => {
                let numBureau = model.extractNumBureau(node);
                let equipe = model.getEquipe(model.findOfficeJson(numBureau));
                if (equipe instanceof Array) equipe = "Equipe Mixte";
                let color = model.getColor(equipe);
                if (equipe !== undefined) dico.set(equipe, color);
            });

            nodes = Array.from(currentMap.getElementsByTagName("rect"));
            nodes.forEach(node => {
                let numBureau = model.extractNumBureau(node);
                let equipe = model.getEquipe(model.findOfficeJson(numBureau));
                if (equipe instanceof Array) equipe = "Equipe Mixte";
                let color = model.getColor(equipe);
                if (equipe !== undefined) dico.set(equipe, color);
            });

            for (let [equipe, color] of dico) {
                if (equipe !== null) {
                    let div = document.createElement("div");
                    div.innerHTML = `
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: ${color}"></div>
                    <h3> : ${equipe}</h3>
                </div>
                `
                    legende.appendChild(div);
                }
            }

            break;
        case "color":
            legende.innerHTML = `
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: purple"></div>
                    <h3> : bureau vide</h3>
                </div>
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: green"></div>
                    <h3> : 1 place libre pour un permanent ou plus</h3>
                </div>
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: #5a9b5a"></div>
                    <h3> : 1 place libre pour un non-permanent</h3>
                </div>
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: #efce9d"></div>
                    <h3> : 0.33 place libre</h3>
                </div>
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: #ffb339"></div>
                    <h3> : stable</h3>
                </div>
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: #ff5858"></div>
                    <h3> : 1 non-permanent en trop</h3>
                </div>
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: red"></div>
                    <h3> : 1 permanent en trop ou plus</h3>
                </div>
            `;
            break;
        default:
            legende.innerHTML = `
                <div class="equipe-legende">
                    <div class="carre-legende" style="background-color: white"></div>
                    <h3> : bureau</h3>
                </div>
                `;

    }

}