"use strict";

import * as model from "../model/model.js";

/**
 * Affiche le svg en mode couleur (vert, orange, rouge)
 * @param bureauJson
 * @param bureauNode
 */
export function display(bureauJson, bureauNode) {
    if (bureauNode != null && bureauJson != null) {
        let nbPersonnel = model.getTauxBureau(bureauJson);
        bureauNode.style.opacity = 0.5;
        setColorAndOpacity(bureauJson.nombre_place - nbPersonnel, bureauNode, bureauJson.nombre_place);
    }
}

function setColorAndOpacity(diff, bureauNode, nbPlace) {
    if (nbPlace === diff && nbPlace !== 0) {
        bureauNode.style.fill = "purple";
        bureauNode.style.opacity = 1;
    } else if (diff >= 1) {
        bureauNode.style.fill = "green";
        bureauNode.style.opacity = 1;
    } else if (diff >= 0.66 && diff < 1) {
        bureauNode.style.fill = "green";
        bureauNode.style.opacity = 0.5;
    } else if (diff >= 0.33 && diff < 0.66) {
        bureauNode.style.fill = "orange";
        bureauNode.style.opacity = 0.3;
    } else if (diff >= -0.5 && diff < 0.5) {
        bureauNode.style.fill = "orange";
        bureauNode.style.opacity = 0.6;
    } else if (diff >= -0.33 && diff < 0) {
        bureauNode.style.fill = "orange";
        bureauNode.style.opacity = 1;
    } else if (diff > -1 && diff < -0.33) {
        bureauNode.style.fill = "red";
        bureauNode.style.opacity = 0.5;
    } else if (diff <= -1) {
        bureauNode.style.fill = "red";
        bureauNode.style.opacity = 1;
    }
}