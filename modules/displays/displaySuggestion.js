"use strict";
import * as model from "../model/model.js";

export function display(text) {
    let autoComplete = document.getElementById("autocomplete");
    autoComplete.innerHTML = "";
    let list = model.autoCompletionPerson(text);
    model.autoCompletionTeam(text).forEach((team) => {
        list.push(team);
    });

    if (list.length === 0) {
        autoComplete.style.display = "none";
        return;
    }

    for (let i = 0; i < 5; i++) {
        if (list[i] === undefined) break;
        let li = document.createElement("li");
        li.textContent = list[i];
        li.addEventListener("click", function () {
            document.getElementById("search").value = list[i];
            autoComplete.style.display = "none";
            document.getElementById("search").focus();
        });
        autoComplete.appendChild(li);
    }
}