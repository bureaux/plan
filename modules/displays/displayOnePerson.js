"use strict";

import * as model from "../model/model.js";

/**
 * Affiche sur la carte en surbrillance le bureau de la personne + les informations du bureau
 * @param personTab
 */
export async function display(personTab) {
    let compteur = document.getElementById("search-bottom");
    let nbFindNode = document.getElementById("search-nb");

    let iterateur;
    if (model.searchIterator === -1) {
        model.setSearchIterator(personTab.length - 1);
        iterateur = model.searchIterator;
    } else if (personTab[model.searchIterator] !== undefined) {
        iterateur = model.searchIterator;
    } else {
        iterateur = 0;
        model.setSearchIterator(0);
    }
    // On récupère les informations du bureau de la personne exemple : "[B101, "Nom Prénom"]" prend "B101"
    let bureau = personTab[iterateur][0];
    // Exemple : "B101" prend "B" -> "b"
    let batiment = bureau[0].toLowerCase();
    // Exemple : "B101" prend "1"
    let etage = bureau[1];

    // On met à jour le modèle
    await model.setAll(batiment, etage, "default", model.year, bureau)
        .then(() => {
            // On récupère le bureau de la personne dans le dom
            let bureauNode = model.findOfficeNode(bureau);

            if (bureauNode !== null) {
                // On en crée un clone pour changer les événements dessus
                let newBureauNode = bureauNode.cloneNode();
                model.focusOneOffice(bureau, newBureauNode, bureauNode);
            } else {
                console.error("No office node found for bureau:", bureau);
            }
        });


    compteur.style.display = "flex";
    nbFindNode.innerHTML = model.searchIterator + 1 + " / " + personTab.length;
    return true;
}