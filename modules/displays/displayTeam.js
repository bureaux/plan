"use strict";
import * as model from "../model/model.js";

/**
 * Affiche le svg en mode team
 * @param bureauJson
 * @param bureauNode
 */
export function display(bureauJson, bureauNode) {
    if (bureauNode != null && bureauJson != null) {
        let equipeDuBureau = model.getEquipe(bureauJson);
        if (equipeDuBureau instanceof Array) equipeDuBureau = "Equipe Mixte";
        bureauNode.style.opacity = 1;
        // Gestion des couleurs des bureaux
        if (equipeDuBureau) {
            bureauNode.style.fill = model.equipes.get(equipeDuBureau);
        }
    }
}