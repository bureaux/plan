"use strict";
import * as displayFormulaire from "../displays/displayFormulaire.js";
import * as model from "../model/model.js";
import * as controlDragAndDrop from "../controllers/controlDragAndDrop.js";
import * as displayNotification from "../displays/displayNotification.js";
import {reset} from "../model/model.js";


export async function display(person, divParent, type, numBureau) {
    return new Promise(async (resolve) => {
        if (model.admin) {
            if (model.checkDate(person)) {
                // div pour une personne
                let div = document.createElement("div");
                div.className = "person";
                div.setAttribute("data-office", numBureau);

                let dateDebut = person.date_debut.split("-")[0] + "/" + person.date_debut.split("-")[1] + "/" + person.date_debut.split("-")[2];
                let dateFin = person.date_fin.split("-")[0] + "/" + person.date_fin.split("-")[1] + "/" + person.date_fin.split("-")[2];

                // récupération du département
                let departement = await model.getDepartement(person);

                // Création de la carte
                let cardContent;
                if (type === "tampon") {
                    cardContent = `
                    <div class="card">
                        <a href="https://annuaire.inria.fr/${person.inria_login}/show" class="card__title" target="_blank">${person.nom} ${person.prenom}</a>
                        <p class="card__content">${person.employeur} - ${person.equipe} - ${departement}</p>
                        ${person.date_fin.split("-")[2] === "2099" ? '<div class="card__date">Permanent</div>' : `<div class="card__date">${dateDebut} - ${dateFin}</div>`}
                    </div>
                `;
                    div.innerHTML = cardContent;
                    div.setAttribute("temp", "true");
                } else {
                    cardContent = `
                    <div class="card">
                        <a href="https://annuaire.inria.fr/${person.inria_login}/show" class="card__title" target="_blank">${person.nom} ${person.prenom}</a>
                        <p class="card__content">${person.employeur} - ${person.equipe} - ${departement}</p>
                        ${person.date_fin.split("-")[2] === "2099" ? '<div class="card__date">Permanent</div>' : `<div class="card__date">${dateDebut} - ${dateFin}</div>`}
                        <div class="card__arrow__right">
                            <img src="./../../img/edit2.png" alt="edit" class="card__arrow__img">
                        </div>
                    </div>
                `;
                    div.innerHTML = cardContent;
                    div.setAttribute("temp", "false");

                    // Ajout d'un événement de clic pour l'édition
                    let divEdit = div.querySelector('.card__arrow__right');
                    divEdit.addEventListener("click", function (event) {
                        displayFormulaire.displayEditPerson(person, numBureau);
                    });

                    let nom = div.querySelector('.card__title');
                    nom.addEventListener("contextmenu", function (event) {
                        // copier le nom et prenom de la personne
                        event.preventDefault();
                        let copyText = nom.textContent;
                        navigator.clipboard.writeText(copyText);
                        displayNotification.displaySuccess("\'" + copyText + "\' a été copié dans le presse-papier");
                    });
                }

                div.draggable = true;
                controlDragAndDrop.initPerson(person, div);

                // Ajout de la carte au parent
                divParent.appendChild(div);
                divParent.style.overflowY = "scroll";
            }
        } else {
            // div pour une personne
            let div = document.createElement("div");
            div.className = "person";
            div.setAttribute("data-office", numBureau);

            // Création de la carte
            div.innerHTML = `
                    <div class="card">
                        <a href="https://annuaire.inria.fr/${person.inria_login}/show" class="card__title" target="_blank">${person.nom} ${person.prenom}</a>
                        <p class="card__content">${person.equipe}</p>
                    </div>
                `;
            div.setAttribute("temp", "false");

            let nom = div.querySelector('.card__title');
            nom.addEventListener("contextmenu", function (event) {
                // copier le nom et prenom de la personne
                event.preventDefault();
                let copyText = nom.textContent;
                navigator.clipboard.writeText(copyText);
                displayNotification.displaySuccess("\'" + copyText + "\' a été copié dans le presse-papier");
            });

            div.draggable = false;

            // Ajout de la carte au parent
            divParent.appendChild(div);
            divParent.style.overflowY = "scroll";
        }
        resolve();
    });
}

export async function displayTeam(divParent, type, numBureau) {
    return new Promise(async (resolve) => {
        let bureau = model.findOfficeJson(numBureau);
        for (const personnel of bureau.personnels) {
            await display(personnel, divParent, type, numBureau);
        }
        resolve();
    });
}