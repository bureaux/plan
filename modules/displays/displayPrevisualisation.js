"use strict";

export function display(divParent, id) {
    let div = document.createElement("div");
    div.className = "person";
    div.id = id;
    div.style.background = "transparent";
    divParent.appendChild(div);
}