"use strict";
import * as viewLogs from "../views/viewLogs.js";
import * as model from "../model/model.js";

let notification = document.getElementById("notification");

export function displayError(text) {
    let divNotif = document.createElement("div");
    divNotif.classList.add("notifications-container");
    divNotif.innerHTML = `
    <div class="error-alert">
    <div class="flex">
      <div class="flex-shrink-0">
        
        <svg aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" class="error-svg">
          <path clip-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z" fill-rule="evenodd"></path>
        </svg>
      </div>
      <div class="error-prompt-container">
        <p class="error-prompt-heading">Erreur
        </p>
        <div class="error-prompt-wrap">
            <p>${text}</p>
        </div>
      </div>
    </div>
  </div>
    `;
    notification.appendChild(divNotif);
    setTimeout(() => {
        divNotif.style.opacity = "0";
        setTimeout(() => {
            divNotif.remove();
        }, 500);
    }, 5000);
}

export function displaySuccess(text) {
    let divNotif = document.createElement("div");
    divNotif.classList.add("notifications-container");
    if (model.admin) {
        divNotif.innerHTML = `
    <div class="success">
        <div class="flex">
            <div class="flex-shrink-0">
                <svg class="succes-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path>
                </svg>
            </div>
            <div class="success-prompt-wrap">
                <p class="success-prompt-heading">Succès</p>
                <div class="success-prompt-prompt">
                    <p>${text}</p>
                </div>
                <div class="success-button-container">
                    <button type="button" class="success-button-main">voir les logs</button>
                </div>
            </div>
        </div>
    </div>
    `;
        divNotif.getElementsByClassName("success-button-main")[0].addEventListener("click", function () {
            viewLogs.update();
        });
    } else {
        divNotif.innerHTML = `
    <div class="success">
        <div class="flex">
            <div class="flex-shrink-0">
                <svg class="succes-svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd"></path>
                </svg>
            </div>
            <div class="success-prompt-wrap">
                <p class="success-prompt-heading">Succès</p>
                <div class="success-prompt-prompt">
                    <p>${text}</p>
                </div>
            </div>
        </div>
    </div>
    `;
    }
    notification.appendChild(divNotif);
    setTimeout(() => {
        divNotif.style.opacity = "0";
        setTimeout(() => {
            divNotif.remove();
        }, 500);
    }, 5000);
}

export function displayWarning(text) {
    let divNotif = document.createElement("div");
    divNotif.classList.add("notifications-container");
    divNotif.innerHTML = `
    <div class="alert">
    <div class="flex">
      <div class="flex-shrink-0">
        <svg aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 alert-svg">
          <path clip-rule="evenodd" d="M8.257 3.099c.765-1.36 2.722-1.36 3.486 0l5.58 9.92c.75 1.334-.213 2.98-1.742 2.98H4.42c-1.53 0-2.493-1.646-1.743-2.98l5.58-9.92zM11 13a1 1 0 11-2 0 1 1 0 012 0zm-1-8a1 1 0 00-1 1v3a1 1 0 002 0V6a1 1 0 00-1-1z" fill-rule="evenodd"></path>
        </svg>
      </div>
      <div class="alert-prompt-wrap">
        <p class="warning-prompt-heading">Attention</p>
        <br>
        <p class="text-sm text-yellow-700">
            ${text}
        </p>
    </div>
  </div>
  </div>
    `;
    notification.appendChild(divNotif);
    setTimeout(() => {
        divNotif.style.opacity = "0";
        setTimeout(() => {
            divNotif.remove();
        }, 500);
    }, 5000);
}