"use strict";
import * as model from "../model/model.js";

/**
 * Affiche sur la carte en surbrillance les bureau de l'équipe
 * @param teamTab
 */
export async function display(teamTab) {
    let compteur = document.getElementById("search-bottom");
    let nbFindNode = document.getElementById("search-nb");
    if (teamTab.length > 0) {
        let iterateur;
        if (model.searchIterator === -1) {
            model.setSearchIterator(teamTab.length - 1);
            iterateur = model.searchIterator;
        } else if (teamTab[model.searchIterator] !== undefined) {
            iterateur = model.searchIterator;
        } else {
            iterateur = 0;
            model.setSearchIterator(0);
        }
        // On récupère les informations du bureau de la personne exemple : "[B101, "Nom Prénom"]" prend "B101"
        let bureau = teamTab[iterateur][0];
        let bureauJson = model.findOfficeJson(bureau);
        // Exemple : "B101" prend "B" -> "b"
        let batiment = bureau[0].toLowerCase();
        // Exemple : variable étage OU "B101" prend "1"
        let etage = bureauJson.etage || bureau[1];

        // On met à jour le modèle
        await model.setAll(batiment, etage, "default", model.year, bureau);

        teamTab[iterateur].forEach(bureau => {
            setTimeout(() => {
                // On récupère le bureau de la personne dans le dom
                let bureauNode = model.findOfficeNode(bureau);
                let newBureauNode;
                // On en crée un clone pour changer les événements dessus
                newBureauNode = bureauNode.cloneNode(true);
                model.focusOneOffice(bureau, newBureauNode, bureauNode);
            }, 100);
        });
        compteur.style.display = "flex";
        nbFindNode.innerHTML = model.searchIterator + 1 + " / " + teamTab.length;
        return true;
    } else {
        return false;
    }
}